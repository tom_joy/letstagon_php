<?php

namespace api\v1\controllers;
use Yii;
use yii\rest\ActiveController;
use yii\db\Query;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\helpers\Json;
use yii\db\Command;
use yii\web\UploadedFile;
use api\v1\models\Events;
use api\v1\models\MasterExperience;
use api\v1\models\User;
use api\v1\models\UserTaggedExperiences;
use api\v1\models\LtgRequestedOpportunities;
use api\v1\models\Cities;
use api\v1\models\States;
use api\v1\models\MasterNgo;
use api\v1\models\Country;
use api\v1\models\Reviews;
use api\v1\models\Booking;

class AdminController extends ActiveController {

    public $modelClass = 'api\v1\models\Events';  // Define a model class name for the controller

    public function behaviors() {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
            ],
        ];
    }

    /*     * **** Event API's Starts Here ****** */

    /* Create Events */

    public function actionInsert_events() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);
        $event_type = $request['event_type'];
        $email=$request['email'];
        $schedule_type = $request['schedule_type'];
        $participation_type = $request['participation_type'];
        $mode = $request['mode'];
        $suitable_for = $request['suitable_for'];
        $no_of_volunteers = $request['no_of_volunteers'];
        $date_scheduled = $request['date_scheduled'];
        $event_duration = $request['event_duration'];
        $state_id = $request['state_id'];
        $city_id = $request['city_id'];
        $address = $request['address'];
        $country_id = $request['country_id'];
        $experience_id = $request['experience_id'];
        $event_info = $request['event_info'];
       
        $status = '1';

        $datee = date('Y-m-d H:i', strtotime($date_scheduled));

        $experience_identity = MasterExperience::find()->where(['id' => $experience_id])->one();
       
        if (empty($experience_identity)) {
            return $result = ['status' => "fail", 'message' => 'No such experience exists'];
        }
        if($event_info == 'private')
        {
            
        $model = new Events();  // Storing new model record in a variable

        $model->event_type = $event_type;
        $model->schedule_type = $schedule_type;
        $model->participation_type = $participation_type;
        $model->mode = $mode;
        $model->suitable_for = $suitable_for;
        $model->no_of_volunteers = $no_of_volunteers;
        $model->date_scheduled = $datee;
        $model->event_duration = $event_duration;
        $model->state_id = $state_id;
        $model->city_id = $city_id;
        $model->address = $address;
        $model->experience_id = $experience_id;
        $model->country_id = $country_id;
        $model->status = $status;
        $model->event_info = $event_info;
        $model->save(false); // To exceute the query
        
        $id=$model->id;
        $date_schedule=$model->date_scheduled;
        $date_time=explode(' ',$date_schedule);
        $event_date=$date_time[0];
        $event_time=$date_time[1];
      
       
       
        $cityid=$model->city_id;
        $cities= Cities::find()->select(['city_name','state_id'])->where(['id'=>$city_id])->one();
        $event_city=$cities['city_name'];
       
        $state_id=$cities['state_id'];
        $states= States::find()->select(['state_name'])->where(['id'=>$state_id])->one();
        $event_state=$states['state_name'];
        $experience=Events::find()->select(['experience_id'])->where(['id'=>$id])->one();
        $exp_id=$experience->experience_id;
        
        $experience_name= MasterExperience::find()->select(['experience_name'])->where(['id'=>$exp_id])->one();
        $event_experience_name=  $experience_name['experience_name'];
     
        $message = "<table border='1' cellpadding='10' cellspacing='10' width='100%' height='100'>
				<tr>
				<td><img class='logo_light' alt='Letstagon Logo' src='http://agathsya.com/letstagon/admin/Admin/assets/images/logo-icon.png'><img class='logo_light' alt='Letstagon Logo' src='http://agathsya.com/letstagon/admin/Admin/assets/images/logo-text.png'></td>
				</tr>
				<tr>
				<td width='100%' height='60'>
				<p></p>
				<p></p>";
        $message .= 'Greetings<br><br>';
        $message .= '<p>You have successfully created an event for an experience "<b>' . $event_experience_name . '" </b></p>';
        $message .= '<p>Its going to held on <b>' . $event_date . '</b> at<b> ' . $event_time . '</b> in <b> ' . $address . ','.$event_city. ',' . $event_state . '</b></p>';
        $message .= '<p>click on the below link  to view an event</p>';
        $message .= '<p><a style="font-size:16px;font-family:Helvetica,Helvetica neue,Arial,Verdana,sans-serif;font-weight:none;color:#ffffff;text-decoration:none;background-color:#3572b0;border-top:11px solid #3572b0;border-bottom:11px solid #3572b0;border-left:20px solid #3572b0;border-right:20px solid #3572b0;border-radius:5px;display:inline-block" class="m_1789912164564125250mobile-button" href="http://staging.letstagon.com/#/detail-description/'.$exp_id.'" target="_blank" data-saferedirecturl="">click here</a></p>';
        $message .= '<p><b>Happy volunteering,</b></p>';
        $message .= '<p><b>LetsTagOn team</b></p>';
        $message .= "
				</td>
				</tr>
				<tr>
				<td width='100%' cellpadding='1' cellspacing='1' bgcolor='#DFDFDF' height='1'>
				<p align='center'>	Website: <a href='http://letstagon.com/'>LetsTagOn</a></font></td>
				</tr>
				</table>";
        Yii::$app->mailer->compose()
                ->setFrom(['support@letstagon.com'=>'LetsTagOn'])
                ->setTo($email)
                ->setSubject('Private oppourtunity-LetsTagOn')
                ->setHtmlBody($message)
                ->send();
        return $result = ['status' => "success", 'message' => 'private event created and mail has sent to particular user'];
    }
    
    else 
    {
       
        $model = new Events();  // Storing new model record in a variable

        $model->event_type = $event_type;
        $model->schedule_type = $schedule_type;
        $model->participation_type = $participation_type;
        $model->mode = $mode;
        $model->suitable_for = $suitable_for;
        $model->no_of_volunteers = $no_of_volunteers;
        $model->date_scheduled = $datee;
        $model->event_duration = $event_duration;
        $model->state_id = $state_id;
        $model->city_id = $city_id;
        $model->address = $address;
        $model->experience_id = $experience_id;
        $model->country_id = $country_id;
        $model->status = $status;
        $model->event_info = $event_info;
        $model->save(false); 
        return $result = ['status' => "success", 'message' => 'Event created successfully'];
    }
}
       
       
    /* View Events */

    public function actionEditevent() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);

        $event_type = $request['event_type'];
        $schedule_type = $request['schedule_type'];
        $participation_type = $request['participation_type'];
        $mode = $request['mode'];
        $suitable_for = $request['suitable_for'];
        $no_of_volunteers = $request['no_of_volunteers'];
        $date_scheduled = $request['date_scheduled'];
        $event_duration = $request['event_duration'];
        $country_id = $request['country_id'];
        $state_id = $request['state_id'];
        $city_id = $request['city_id'];
        $address = $request['address'];
        $experience_id = $request['experience_id'];
        $status = '1';
        $event_info = $request['event_info'];
        $event_id = $request['event_id'];

        $datee = date('Y-m-d H:i', strtotime($date_scheduled));

        $model = Events::find()->where(['id' => $event_id])->one();
        if (empty($model)) {
            return $result = ['status' => 'fail', 'message' => 'No such events'];
        }

        $master = MasterExperience::find()->where(['id' => $experience_id])->one();
        if (empty($master)) {
            return $result = ['status' => 'fail', 'message' => 'No such experience'];
        }

        $model->event_type = $event_type;
        $model->schedule_type = $schedule_type;
        $model->participation_type = $participation_type;
        $model->mode = $mode;
        $model->suitable_for = $suitable_for;
        $model->no_of_volunteers = $no_of_volunteers;
        $model->date_scheduled = $datee;
        $model->event_duration = $event_duration;
        $model->state_id = $state_id;
        $model->city_id = $city_id;
        $model->address = $address;
        $model->country_id = $country_id;
        $model->experience_id = $experience_id;
        $model->status = $status;
        $model->event_info = $event_info;

        $model->save(false);
        return $result = ['status' => 'success', 'message' => 'Event Updated Successfully'];
    }

    /* Api to Delete About data */

    public function actionDelete_event() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);

        $event_id = $request['event_id'];

        $model = Events::find()->where(['id' => $event_id])->one();
        if (empty($model)) {
            return $result = ['status' => 'fail', 'message' => 'No data'];
        }
        $model->delete();
        return $result = ['status' => 'success', 'message' => 'Event Deleted Successfully'];
    }

    /* Api to View data */

    public function actionEvent_list() {
        $image_path = "http:///oppurtunity_images/";

        $eventlist = "SELECT e.id,e.event_type,e.schedule_type,e.participation_type,e.mode,e.suitable_for,e.event_info,
						e.no_of_volunteers,e.date_scheduled,e.event_duration,e.address,me.experience_name,me.cause,
						me.description,me.image_path,s.state_name,c.city_name,
						(SELECT count(*) FROM user_tagged_experiences as ut WHERE e.id = ut.event_id) AS user_tag_count,country.country_name 
						FROM events as e JOIN master_experience as me ON 
							e.experience_id = me.id  
								JOIN states as s ON 
								e.state_id = s.id 
								JOIN country as country ON 
								e.country_id = country.id 
									JOIN cities as c ON 
									e.city_id = c.id  and e.event_info='public' order by id desc";
        $command = Yii::$app->db->createCommand($eventlist);
        $events = $command->queryAll();
        

        if (empty($events)) {
            return $result = ['status' => 'fail', 'message' => 'No data'];
        }

        $array = [];
        $i = 1;
        foreach ($events as $event) 
		{
            $description = substr($event['description'], 0, 30);
            $out = [
                'index' => $i,
                'id' => $event['id'],
                'event_type' => $event['event_type'],
                'schedule_type' => $event['schedule_type'],
                'participation_type' => $event['participation_type'],
                'mode' => $event['mode'],
                'no_of_volunteers' => $event['no_of_volunteers'],
                'user_tag_count' => $event['user_tag_count'],
                'date_scheduled' => date('d F Y H:i', strtotime($event['date_scheduled'])),
                'event_duration' => $event['event_duration'],
                'address' => $event['address'],
                'experience_name' => $event['experience_name'],
                'cause' => $event['cause'],
                'description' => $description,
                'image_path' => $image_path . '' . $event['image_path'],
                'state_name' => $event['state_name'],
                'city_name' => $event['city_name'],
                'country_name' => $event['country_name'],
                'event_info' => $event['event_info']
            ];
            $i++;
            array_push($array, $out);
			
        }

        return $result = ['content' => $array, 'status' => 'success', 'message' => 'list of events'];
    }

    public function actionEvent_details() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);

        $event_id = $request['event_id'];

        $image_path = "http://localhost:8888/LetsTagon/oppurtunity_images/";

        $event_info = Events::find()->where(['id' => $event_id])->one();

        if (empty($event_info)) {
            return $result = ['status' => 'fail', 'message' => 'No Such Events'];
        }

        $eventlist = "SELECT e.id,me.id as experience_id,e.event_type,e.schedule_type,e.participation_type,e.mode,e.event_info,
				     e.no_of_volunteers,e.date_scheduled,e.suitable_for,e.event_duration,e.address,me.experience_name,me.cause,
						me.description,me.image_path,s.state_name,c.city_name,e.state_id,e.city_id,e.country_id
						FROM events as e JOIN master_experience as me ON 
							e.experience_id = me.id  
								JOIN states as s ON 
								e.state_id = s.id 
									JOIN cities as c ON 
									e.city_id = c.id  where e.id = $event_id";
        $command = Yii::$app->db->createCommand($eventlist);
        $events = $command->queryAll();

        $event = $events[0];

        $out = [
            'event_id' => $event['id'],
            'experience_id' => $event['experience_id'],
            'event_type' => $event['event_type'],
            'schedule_type' => $event['schedule_type'],
            'participation_type' => $event['participation_type'],
            'mode' => $event['mode'],
            'no_of_volunteers' => $event['no_of_volunteers'],
            'date_scheduled' => date('d F Y H:i', strtotime($event['date_scheduled'])),
            'event_duration' => $event['event_duration'],
            'address' => $event['address'],
            'state_id' => $event['state_id'],
            'city_id' => $event['city_id'],
            'country_id' => $event['country_id'],
            'suitable_for' => $event['suitable_for'],
            'event_info' => $event['event_info'],
        ];


        return $result = ['content' => $out, 'status' => 'success', 'message' => 'Event details'];
    }

    public function actionEvent_details_view_admin() {

        $post = file_get_contents("php://input");
        $request = Json::decode($post);

        $event_id = $request['event_id'];


        if (empty($event_id)) {
            return $result = ['status' => "fail", 'message' => 'Event parameter is missing'];
        }


        $eventlist = "SELECT e.id,me.id as experience_id,e.event_type,e.schedule_type,e.participation_type,e.mode,
						e.no_of_volunteers,e.date_scheduled,
						e.event_duration,e.address,e.status,me.experience_name,country.country_name,
						s.state_name,c.city_name,e.state_id,e.city_id
						FROM events as e JOIN master_experience as me ON 
							e.experience_id = me.id  
								JOIN states as s ON 
								e.state_id = s.id 
								JOIN country as country ON 
								e.country_id = country.id
							        JOIN cities as c ON 
									e.city_id = c.id  where e.id = $event_id";
        $command = Yii::$app->db->createCommand($eventlist);
        $events = $command->queryAll();

        if (empty($events)) {
            return $result = ['status' => 'fail', 'message' => 'No event data'];
        }

        $event = $events[0];

        $event_id = $event['id'];
        $experience_name = $event['experience_name'];
        $event_type = $event['event_type'];
        $schedule_type = $event['schedule_type'];
        $participation_type = $event['participation_type'];
        $mode = $event['mode'];
        $date_scheduled = date('d F Y H:i', strtotime($event['date_scheduled']));
        $event_duration = $event['event_duration'];
        $no_of_volunteers = $event['no_of_volunteers'];
        $address = $event['address'];
        $state_name = $event['state_name'];
        $city_name = $event['city_name'];
        $event_status = $event['status'];
        $country_name = $event['country_name'];

        if ($event_status != '1') {
            $event_status = 'De-Active';
        } else {
            $event_status = 'Active';
        }


        $userlist = "SELECT ut.id as user_tag_id,ut.created_at,ut.attendance_flag,u.first_name,u.last_name,u.email,u.phone,
		e.id as event_id,e.experience_id,e.date_scheduled,e.address
						FROM user_tagged_experiences as ut
                          	JOIN user as u ON 
							ut.user_id = u.id
                                JOIN events as e ON 
                                ut.event_id = e.id 
                                    where e.id = '$event_id'
                                        order by ut.created_at desc";
        $command = Yii::$app->db->createCommand($userlist);
        $user_tagged = $command->queryAll();
        // print_r($events);exit;

        /* 	if(empty($events))
          {
          return $result = ['status'=>'fail','message'=>'No data'];
          }
         */
        $array = [];
        $i = 1;
        foreach ($user_tagged as $user_tag) {
            $first_name = $user_tag['first_name'];
            $last_name = $user_tag['last_name'];
            $created_at = $user_tag['created_at'];

            $applied_date = date('d F Y H:i', strtotime($created_at));

            $user_name = $first_name . ' ' . $last_name;
            $out = [
                'index' => $i,
                'id' => $user_tag['user_tag_id'],
                'user_name' => $user_name,
                'email' => $user_tag['email'],
                'phone' => $user_tag['phone'],
                'applied_on' => $applied_date,
                'attendance_flag' => $user_tag['attendance_flag']
            ];

            array_push($array, $out);
            $i++;
        }

        return $result = ['experience_name' => $experience_name, 'event_id' => $event_id, 'event_type' => $event_type, 'schedule_type' => $schedule_type, 'participation_type' => $participation_type, 'mode' => $mode, 'date_scheduled' => $date_scheduled, 'event_duration' => $event_duration, 'no_of_volunteers' => $no_of_volunteers, 'address' => $address, 'state_name' => $state_name, 'city_name' => $city_name, 'country_name' => $country_name, 'event_status' => $event_status, 'content' => $array, 'status' => 'success', 'message' => 'list of events'];
    }

    /*     * **** EVENT API's Ends Here ****** */


    /* Api to Join Event and master View data for Limit of 3 in Home page */

    public function actionExperience_list_limit_old() {
        $image_path = "http://staging.api.letstagon.com/oppurtunity_images/";

        $eventlist = "SELECT e.id,me.id as expid,e.event_type,e.schedule_type,e.participation_type,e.mode,
						e.no_of_volunteers,e.date_scheduled,e.address,me.experience_name,me.cause,
						me.description,me.image_path,s.state_name,c.city_name
						FROM events as e
                          JOIN master_experience as me ON 
							e.experience_id = me.id  
								JOIN states as s ON 
								e.state_id = s.id 
									JOIN cities as c ON 
									e.city_id = c.id order by id desc limit 3";
        $command = Yii::$app->db->createCommand($eventlist);
        $events = $command->queryAll();
        // print_r($events);exit;

        if (empty($events)) {
            return $result = ['status' => 'fail', 'message' => 'No data'];
        }

        $array = [];

        foreach ($events as $event) {
            $out = [
                'id' => $event['id'],
                'experienceid' => $event['expid'],
                'event_type' => $event['event_type'],
                'schedule_type' => $event['schedule_type'],
                'participation_type' => $event['participation_type'],
                'mode' => $event['mode'],
                'no_of_volunteers' => $event['no_of_volunteers'],
                'address' => $event['address'],
                'experience_name' => ucfirst($event['experience_name']),
                'cause' => $event['cause'],
                'description' => $event['description'],
                'image_path' => $image_path . '' . $event['image_path'],
                'state_name' => $event['state_name'],
                'city_name' => $event['city_name']
            ];

            array_push($array, $out);
        }

        return $result = ['content' => $array, 'status' => 'success', 'message' => 'list of events'];
    }

    public function actionExperience_list_limit() {

        $image_path = "http://staging.api.letstagon.com/oppurtunity_images/";

        $eventlist = "SELECT events.experience_id, me.id as expid,me.experience_name,
						me.description,me.category,me.image_path,events.date_scheduled,
							events.address,s.state_name,c.city_name,events.event_type,events.schedule_type,
								events.participation_type,events.mode, 
									events.id as event_id,events.status as event_status,me.status as master_status
						FROM events INNER JOIN 
					( SELECT events.experience_id, min(date_scheduled) AS maxDate FROM events where events.date_scheduled >= CURDATE() GROUP BY events.experience_id) AS Tmp ON 
						events.experience_id = Tmp.experience_id AND 
							events.date_scheduled = Tmp.maxDate 
								JOIN master_experience as me ON events.experience_id = me.id 
								JOIN states as s ON  events.state_id = s.id 
								JOIN cities as c ON  events.city_id = c.id
								where events.status=1 and me.status=1 ORDER by events.date_scheduled asc limit 3";
        $command = Yii::$app->db->createCommand($eventlist);
        $events = $command->queryAll();
        // print_r($events);exit;

        if (empty($events)) {
            return $result = ['status' => 'fail', 'message' => 'No data'];
        }

        $array = [];




        foreach ($events as $event) {
            $description = $event['description'];
			$description_substr = strlen($description) > 100 ? substr($description,0,100)."..." : $description; 
			
			$experience_name = $event['experience_name'];
			$experience_substr = strlen($experience_name) > 25 ? substr($experience_name,0,25)."..." : $experience_name;
            $experience_id = $event['expid'];
            $sum = Reviews::find()->where(['experience_id' => $experience_id])->sum('rating');
            $count = Reviews::find()->where(['experience_id' => $experience_id])->count();
            if ($count == 0) {
                $total_stars = 0;
            } else {
                $total_stars = $sum / $count;
            }




            $out = [
                'id' => $event['event_id'],
                'experienceid' => $event['expid'],
                'experience_name' => $experience_substr,
                'event_type' => $event['event_type'],
                'schedule_type' => $event['schedule_type'],
                'participation_type' => $event['participation_type'],
                'mode' => $event['mode'],
                'date_scheduled' => date('d F Y', strtotime($event['date_scheduled'])),
                'address' => $event['address'],
                'state_name' => $event['state_name'],
                'city_name' => $event['city_name'],
                'description' => $description_substr,
                'image_path' => $image_path . '' . $event['image_path'],
                'review' => $total_stars
            ];

            array_push($array, $out);
        }

        return $result = ['content' => $array, 'status' => 'success', 'message' => 'list of events'];
    }

    /* 	
      public function actionExperience_list()
      {
      $image_path = "http://staging.api.letstagon.com/oppurtunity_images/";

      $eventlist = "SELECT events.experience_id, me.id,me.experience_name,me.description,me.category,me.image_path,events.date_scheduled,
      events.address,s.state_name,c.city_name,events.event_type,events.schedule_type,
      events.participation_type,events.mode,
      events.id as event_id,events.status as event_status,me.status as master_status
      FROM events INNER JOIN
      ( SELECT events.experience_id, min(date_scheduled) AS maxDate FROM events where events.date_scheduled >= CURDATE() GROUP BY events.experience_id ) AS Tmp ON
      events.experience_id = Tmp.experience_id AND
      events.date_scheduled = Tmp.maxDate
      JOIN master_experience as me ON events.experience_id = me.id
      JOIN states as s ON  events.state_id = s.id
      JOIN cities as c ON  events.city_id = c.id
      where events.status=1 and me.status=1 order by events.date_scheduled asc";
      $command = Yii::$app->db->createCommand($eventlist);
      $events = $command->queryAll();
      // print_r($events);exit;

      if(empty($events))
      {
      return $result = ['status'=>'fail','message'=>'No data'];
      }

      $array = [];

      foreach($events as $event)
      {
      $description	= substr($event['description'],0,100).'..';
      $out = [
      'id'=>$event['id'],
      'experience_name'=>$event['experience_name'],
      'event_type'=>$event['event_type'],
      'schedule_type'=>$event['schedule_type'],
      'participation_type'=>$event['participation_type'],
      'mode'=>$event['mode'],
      'date_scheduled'=> date('d F Y', strtotime($event['date_scheduled'])),
      'address'=>$event['address'],
      'state_name'=>$event['state_name'],
      'city_name'=>$event['city_name'],
      'description'=>$description,
      'image_path'=>$image_path.''.$event['image_path']
      ];

      array_push($array,$out);

      }

      return $result = ['content'=>$array,'status'=>'success','message'=>'list of events'];
      }
     */

     public function actionExperience_list()
{

$post = file_get_contents("php://input");
$request = Json::decode($post);

if(isset($request['city']))
{ 
$city_id = $request['city'];
} 
else
{
$city_id = "";
}

if(isset($request['category']))
{ 
$category = $request['category'];
} 
else
{
$category = "";
}

if(isset($request['events']))
{ 
$event_name = $request['events'];
} 
else
{
$event_name = "";
}


$image_path = "http://staging.api.letstagon.com/oppurtunity_images/";

$eventlist = "SELECT events.experience_id, me.id,me.experience_name,me.description,me.category,me.image_path,events.date_scheduled,
events.address,s.state_name,c.city_name,events.event_type,events.schedule_type,
events.participation_type,events.mode, 
events.id as event_id,events.status as event_status,me.status as master_status
FROM events INNER JOIN 
( SELECT events.experience_id, min(date_scheduled) AS maxDate FROM events where events.date_scheduled >= CURDATE() GROUP BY events.experience_id ) AS Tmp ON 
events.experience_id = Tmp.experience_id AND 
events.date_scheduled = Tmp.maxDate 
JOIN master_experience as me ON events.experience_id = me.id 
JOIN states as s ON events.state_id = s.id 
JOIN cities as c ON events.city_id = c.id
where events.status=1 and me.status=1";

if(!empty($event_name))
{
$eventlist .= "
AND me.experience_name LIKE '%".$event_name."%'";
}

if(!empty($city_id))
{
$eventlist .= "
AND events.city_id = '".$city_id."'";
}

if(!empty($category))
{
$eventlist .= "
AND me.category = '".$category."'";
}


$command = Yii::$app->db->createCommand($eventlist);
$events = $command->queryAll();
// print_r($events);exit;

if(empty($events))
{
return $result = ['status'=>'fail','message'=>'No data'];
}

$array = [];

foreach($events as $event)
{
$description	= substr($event['description'],0,100).'..';
$experience_id	=	$event['id'];
$sum = Reviews::find()->where(['experience_id'=>$experience_id])->sum('rating');
$count = Reviews::find()->where(['experience_id'=>$experience_id])->count();
if($count == 0){
$total_stars = 0;
}
else{
$total_stars = $sum/$count;
}

$out = [
'id'=>$event['id'],
'experience_name'=>$event['experience_name'],
'category'=>$event['category'],
'event_type'=>$event['event_type'],
'schedule_type'=>$event['schedule_type'],
'participation_type'=>$event['participation_type'],
'mode'=>$event['mode'],
'date_scheduled'=> date('d F Y H:i', strtotime($event['date_scheduled'])), 
'address'=>$event['address'],
'state_name'=>$event['state_name'],
'city_name'=>$event['city_name'],
'description'=>$description,
'event_id'=>$event['event_id'],
'image_path'=>$image_path.''.$event['image_path'],
'review'=>$total_stars
];

array_push($array,$out);

}

return $result = ['content'=>$array,'status'=>'success','message'=>'list of events'];
}
    /*     * ***** Api to Join Event and master View data for the particular experience with events ***** */

    public function actionExperience_detailed_view() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);

        $experience_id = $request['experience_id'];


        if (empty($experience_id)) {
            return $result = ['status' => "fail", 'message' => 'Experience parameter is missing'];
        }

        $image_path = "http://staging.api.letstagon.com/oppurtunity_images/";

        $experience = MasterExperience::find()->where(['id' => $experience_id])->one();
		
        if (empty($experience)) {
            return $result = ['status' => 'fail', 'message' => 'No experience data'];
        }
        $ngodetails = MasterNgo::find()->select(['description','ngo_name'])->where(['id' => $experience->ngo_id])->one();
        $schedule_type = Events::find()->select(['schedule_type', 'participation_type', 'event_type'])->where(['experience_id' => $experience_id])->one();
		$experience_name = $experience->experience_name;
		;
		
        $cause = $experience->cause;
        $category = $experience->category;
		
        $description = $experience->description;
		
        $file = $image_path . $experience->image_path;
        $ngo_description=$ngodetails->description;
        $ngo_name=$ngodetails->ngo_name;
        return $result = ['experience_name' => $experience_name, 'cause' => $cause, 'category' => $category, 'description' => $description, 'image_path' => $file, 'ngo_name'=>$ngo_name,'ngo_description' => $ngo_description, 'event_schedule' => $schedule_type->schedule_type, 'participation_type' => $schedule_type->participation_type, 'event_type' => $schedule_type->event_type, 'status' => 'success', 'message' => 'list of events'];
    }

    public function actionExperience_detailed_view_admin() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);

        $experience_id = $request['experience_id'];


        if (empty($experience_id)) {
            return $result = ['status' => "fail", 'message' => 'Experience parameter is missing'];
        }

        $image_path = "http://staging.api.letstagon.com/oppurtunity_images/";

        $experience = MasterExperience::find()->where(['id' => $experience_id])->one();
        if (empty($experience)) {
            return $result = ['status' => 'fail', 'message' => 'No experience data'];
        }
        $ngo_detail = MasterNgo::find()->select(['ngo_name'])->where(['id' => $experience->ngo_id])->scalar();

        $experience_name = $experience->experience_name;
        $cause = $experience->cause;
        $category = $experience->category;
        $description = $experience->description;
        $file = $image_path . $experience->image_path;
        $exp_status = $experience->status;

        if ($exp_status != '1') {
            $exp_status = 'De-Active';
        } else {
            $exp_status = 'Active';
        }


        $eventlist = "SELECT e.id,e.event_type,e.schedule_type,e.participation_type,e.mode,
						e.no_of_volunteers,e.date_scheduled,e.event_duration,
						e.address,e.status,me.experience_name,
						s.state_name,c.city_name,
						(SELECT count(*) FROM user_tagged_experiences as ut WHERE e.id = ut.event_id) AS user_tag_count 
						FROM events as e
                          JOIN master_experience as me ON 
							e.experience_id = me.id  
								JOIN states as s ON 
								e.state_id = s.id 
									JOIN cities as c ON 
									e.city_id = c.id where me.id = $experience_id order by date_scheduled desc";
        $command = Yii::$app->db->createCommand($eventlist);
        $events = $command->queryAll();
        // print_r($events);exit;

        /* if(empty($events))
          {
          return $result = ['status'=>'fail','message'=>'No data'];
          } */

        $array = [];
        $i = 1;
        foreach ($events as $event) {
            $out = [
                'index' => $i,
                'id' => $event['id'],
                'experience_name' => $event['experience_name'],
                'event_type' => $event['event_type'],
                'schedule_type' => $event['schedule_type'],
                'participation_type' => $event['participation_type'],
                'mode' => $event['mode'],
                'no_of_volunteers' => $event['no_of_volunteers'],
                'date_scheduled' => $event['date_scheduled'],
                'event_duration' => $event['event_duration'],
                'address' => $event['address'],
                'state_name' => $event['state_name'],
                'city_name' => $event['city_name'],
                'user_tag_count' => $event['user_tag_count'],
                'status' => $event['status'],
            ];

            array_push($array, $out);
            $i++;
        }

        return $result = ['experience_name' => $experience_name, 'cause' => $cause, 'category' => $category, 'description' => $description, 'image_path' => $file, 'exp_status' => $exp_status, 'ngo_name' => $ngo_detail, 'content' => $array, 'status' => 'success', 'message' => 'list of events'];
    }

    /*     * **** Master Experience API's Starts Here ****** */

    public function actionCreate_master_experience() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);
        $name = $_POST['experience_name'];
        $description = $_POST['description'];
        $cause = $_POST['cause'];
        $category = $_POST['category'];
        $ngo_id = $_POST['ngo_id'];
        $model = new MasterExperience();
        $images = UploadedFile::getInstancesByName("file");
        if (empty($images)) {
            return $result = ['status' => "fail", 'message' => "File not found"];
        }
        foreach ($images as $image) {

            $filename = $image->name;
            $path = "../../oppurtunity_images/" . $filename;
            $image->saveAs($path); //saving image
        }
        $model->experience_name = $name;
        $model->description = $description;
        $model->cause = $cause;
        $model->image_path = $filename;
        $model->category = $category;
        $model->status = 1;
        $model->ngo_id = $ngo_id;
        $model->save(false);
        return $result = ['status' => 'success', 'message' => 'Experience created'];
    }

    /* Api to View list of experiences */

    public function actionMaster_experience_list() {
        $imagepath = "http://staging.api.letstagon.com/oppurtunity_images/";
        $array = [];
        $experience_list = "SELECT me.id,me.experience_name,me.cause,me.category,me.description,me.image_path,me.created_at,ngo.ngo_name,ngo.description as ngo_description,
						(SELECT count(*) FROM events as e WHERE me.id = e.experience_id) AS event_count 
							FROM master_experience as me 
							JOIN master_ngo as ngo ON me.ngo_id = ngo.id order by me.id desc";

        $command = Yii::$app->db->createCommand($experience_list);
        $experience_res = $command->queryAll();

        if (empty($experience_res)) {
            return $result = ['status' => 'fail', 'message' => 'No data'];
        }
        $i = 1;
        foreach ($experience_res as $experience) {
            $out = [
                'index' => $i,
                'experience_id' => $experience['id'],
                'name' => $experience['experience_name'],
                'cause' => $experience['cause'],
                'category' => $experience['category'],
                'description' => $experience['description'],
                'created_at' => date('d F Y', strtotime($experience['created_at'])),
                'image' => $imagepath . $experience['image_path'],
                'event_count' => $experience['event_count'],
                'ngo_name' => $experience['ngo_name'],
                'ngo_description' => $experience['ngo_description']
            ];
            $i++;
            array_push($array, $out);
        }

        return $result = ['content' => $array, 'status' => 'success', 'message' => 'list of experiences'];
    }

    /* Api to Delete experience */

    public function actionDelete_experience() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);
        $id = $request['experience_id'];
        $experience = MasterExperience::find()->where(['id' => $id])->one();
        if (empty($experience)) {
            return $result = ['status' => 'fail', 'message' => 'No data'];
        }
        $experience->delete();
        return $result = ['status' => 'success', 'message' => 'Deleted Successfully'];
    }

    //For getting particular Experience details
    public function actionExperience_id() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);
        $id = $request['experience_id'];
        $array = [];
        $imagepath = "http://staging.api.letstagon.com/oppurtunity_images/";
        $experience = MasterExperience::find()->where(['id' => $id])->one();
        if (empty($experience)) {
            return $result = ['status' => 'fail', 'message' => 'No data'];
        }
        $ngo_details = MasterNgo::find()->select(['id'])->where(['id' => $experience->ngo_id])->one();
        $out = [
            'experience_name' => $experience->experience_name,
            'cause' => $experience->cause,
            'description' => $experience->description,
            'file' => $imagepath . $experience->image_path,
            'category' => $experience->category,
            'ngo_id' => $ngo_details->id
        ];


        return $result = ['data' => $out, 'status' => 'success', 'message' => 'View of particular experience'];
    }

    /* Api to Edit Experience */

    public function actionEdit_master_experience() 
    {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);

        $name = $_POST['experience_name'];
        $description = $_POST['description'];
        $cause = $_POST['cause'];
        $category = $_POST['category'];
        $id = $_POST['id'];
        $ngo_id = $_POST['ngo_id'];
        $model = MasterExperience::find()->where(['id' => $id])->one();
        if (empty($model)) {
            return $result = ['status' => 'fail', 'message' => 'No record Found'];
        }
        $images = UploadedFile::getInstancesByName("file");
        if (!empty($images)) {
            foreach ($images as $image) {

                $filename = $image->name;
                $path = "../../oppurtunity_images/" . $filename;
                $image->saveAs($path); //saving image
            }
        } else {
            $filename = $model->image_path;
        }

        $model->experience_name = $name;
        $model->description = $description;
        $model->cause = $cause;
        $model->category = $category;
        $model->image_path = $filename;
        $model->ngo_id = $ngo_id;
        $model->save(false);
        return $result = ['status' => 'success', 'message' => 'Updated Successfully'];
    }

    /*     * **** API to TagOn the user to the particular event  ****** */

    public function actionUsertagged() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);
        $event_id = $request['event_id'];
        $user_id = $request['user_id'];
        $identity = User::find()->select(['email', 'first_name', 'last_name'])->where(['id' => $user_id])->one();
       
        if (empty($identity)) {
            return $result = ['status' => "fail", 'message' => 'user not exists'];
        }

        $user_allocated = UserTaggedExperiences::find()->where(['user_id' => $user_id, 'event_id' => $event_id])->one();
        if (!empty($user_allocated)) {
            return $result = ['status' => "fail", 'message' => 'Sorry.. You already applied for this opportunity'];
        }

        /* To get the particular event details */

        $event_details_query = "SELECT e.id,e.event_type,e.schedule_type,e.participation_type,e.mode,
						e.no_of_volunteers,e.date_scheduled,e.address,me.experience_name,me.cause,
						me.description,me.image_path,s.state_name,c.city_name
						FROM events as e JOIN master_experience as me ON 
							e.experience_id = me.id  
								JOIN states as s ON 
								e.state_id = s.id 
									JOIN cities as c ON 
									e.city_id = c.id where e.id = $event_id order by id asc";

        $command = Yii::$app->db->createCommand($event_details_query);
        $event_details_array = $command->queryAll();
        

        if (empty($event_details_array)) {
            return $result = ['status' => 'fail', 'message' => 'No data'];
        }

        $event_details = $event_details_array[0];

        $event_sch_date = $event_details['date_scheduled'];
        
        $event_datetime= explode(' ', $event_sch_date);
        
        $event_time=$event_datetime[1];
       
        $event_date = date('d F Y', strtotime($event_sch_date));
       
        $event_experience_name = $event_details['experience_name'];
        $event_address = $event_details['address'];
        $event_state = $event_details['state_name'];
        $event_city = $event_details['city_name'];

        /* To get the particular event details ends */

        $first_name = $identity->first_name;
        $last_name = $identity->last_name;
        $user_email = $identity->email;
        $user_name = $first_name . ' ' . $last_name;

        $model = new UserTaggedExperiences();
        $model->event_id = $event_id;
        $model->user_id = $user_id;
        $model->save(false);

        $message = "<table border='1' cellpadding='10' cellspacing='10' width='100%' height='100'>
				<tr>
				<td><img class='logo_light' alt='Letstagon Logo' src='http://agathsya.com/letstagon/admin/Admin/assets/images/logo-icon.png'><img class='logo_light' alt='Letstagon Logo' src='http://agathsya.com/letstagon/admin/Admin/assets/images/logo-text.png'></td>
				</tr>
				<tr>
				<td width='100%' height='60'> 
				<p></p>";
        $message .= 'Greetings <b>' . $user_name . ',</b><br><br>';
        $message .= '<p>You have successfully tagged for the experience "<b>' . $event_experience_name . '" </b></p>';
        $message .= '<p>Its going to held on <b>' . $event_date . '</b> at<b> ' . $event_time . '</b> in <b> ' . $event_city . ', ' . $event_state . '</b></p>';
        $message .= '<p></p>';
        $message .= '<p></p>';
        $message .= '<p><b>Happy volunteering,</b></p>';
        $message .= '<p><b>LetsTagOn team</b></p>';

        $message .= "
				</td>
				</tr>
				<tr>
				<td width='100%' cellpadding='1' cellspacing='1' bgcolor='#DFDFDF' height='1'>
				<p align='center'>	Website: <a href='http://letstagon.com/'>LetsTagOn</a></font></td>
				</tr>
				</table>";


        //mail code starts
        Yii::$app->mailer->compose()
                ->setFrom(['support@letstagon.com'=>'LetsTagOn'])
                ->setTo($user_email)
                ->setSubject('Tagged Successfully')
                ->setHtmlBody($message)
                ->send();


        return $result = ['status' => 'success', 'message' => 'Congratulations! You are Tagged successfully'];
    }


    /* Api to Join user applied with event and master experience for table view data */

    public function actionApplied_opportunity_list() {
        $opp_list = "SELECT ut.id as user_tag_id,ut.created_at,ut.attendance_flag,u.first_name,u.last_name,u.email,u.phone,
		e.id as event_id,e.experience_id,e.date_scheduled,e.mode,
		e.event_duration,me.experience_name,me.cause,
        	e.address,s.state_name,c.city_name FROM user_tagged_experiences as ut
                          	JOIN user as u ON 
							ut.user_id = u.id
                                JOIN events as e ON 
                                ut.event_id = e.id
                                	JOIN master_experience as me ON 
									e.experience_id = me.id
                                        JOIN states as s ON 
										e.state_id = s.id 
											JOIN cities as c ON 
											e.city_id = c.id
                                            order by ut.created_at desc";
        $command = Yii::$app->db->createCommand($opp_list);
        $opp_list_result = $command->queryAll();

        // print_r($events);exit;

        if (empty($opp_list_result)) {
            return $result = ['status' => 'fail', 'message' => 'No data'];
        }

        $array = [];

        $i = 1;
        foreach ($opp_list_result as $opp_res) {
            $out = [
                'index' => $i,
                'user_tag_id' => $opp_res['user_tag_id'],
                'user_name' => $opp_res['first_name'] . ' ' . $opp_res['last_name'],
                'phone' => $opp_res['phone'],
                'experience_name' => $opp_res['experience_name'],
                'cause_name' => $opp_res['cause'],
                'mode' => $opp_res['mode'],
                'event_date' => date('d F Y H:m', strtotime($opp_res['date_scheduled'])),
                'event_duration' => $opp_res['event_duration'],
                'address' => $opp_res['address'],
                'state_name' => $opp_res['state_name'],
                'city_name' => $opp_res['city_name'],
                'attendance_flag' => $opp_res['attendance_flag'],
                'event_id' => $opp_res['event_id']
            ];

            array_push($array, $out);

            $i++;
        }

        return $result = ['content' => $array, 'status' => 'success', 'message' => 'list of volunteers tagged'];
    }

    /* Api to update the the attendance flag for the volunteer */

    public function actionUpdate_tagged_status() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);

        $user_tag_id = $request['user_tag_id'];
        $attendance_flag = $request['attendance_flag'];


        $model = UserTaggedExperiences::find()->where(['id' => $user_tag_id])->one();
        if (empty($model)) {
            return $result = ['status' => 'fail', 'message' => 'No record Found'];
        }

        $model->attendance_flag = $attendance_flag;
        $model->save(false);

        return $result = ['status' => 'success', 'message' => 'Attendence Updated Successfully'];
    }

    public function actionSelf_attendance() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);

        $user_tag_id = $request['user_tag_id'];
        $attendance_flag = $request['attendance_flag'];
        $userid = $request['user_id'];

        $model = UserTaggedExperiences::find()->where(['id' => $user_tag_id, 'user_id' => $userid])->one();
        if (empty($model)) {
            return $result = ['status' => 'fail', 'message' => 'No record Found'];
        }
        $model->attendance_flag = $attendance_flag;
        $model->save(false);

        return $result = ['status' => 'success', 'message' => 'Attendece Updated Successfully'];
    }

    public function actionVolunteers_list() {
        $volunteers = User::find()->select(['id', 'first_name', 'last_name', 'email', 'phone', 'dob', 'designation', 'organization', 'role_type', 'status', 'created_at'])->where(['role_type' => '2'])->orderBy('id desc')->all();
        if (empty($volunteers)) {
            return $result = ['status' => 'fail', 'message' => 'No data'];
        }

        $array = [];
        $i = 1;
        foreach ($volunteers as $volunteer) {
            $out = [
                'index' => $i,
                'id' => $volunteer['id'],
                'first_name' => $volunteer['first_name'],
                'last_name' => $volunteer['last_name'],
                'phone' => $volunteer['phone'],
                'email' => $volunteer['email'],
                'dob' => $volunteer['dob'],
                'organisation' => $volunteer['organization'],
                'role_type' => $volunteer['role_type'],
                'status' => $volunteer['status'],
                'created_at' => $volunteer['created_at'],
                'designation' => $volunteer['designation']
            ];

            array_push($array, $out);
            $i++;
        }
        return $result = ['content' => $array, 'status' => 'success', 'message' => 'list of volunteers'];
    }

    public function actionRequested_opportunity_list() {
        $opp_list = "SELECT rq.id as request_id,rq.created_on,rq.status_flag,rq.organisation,rq.request_date,rq.no_of_volunteers,rq.description,u.first_name,u.last_name,u.email,u.phone,e.event_info,
		e.id as event_id,e.experience_id,e.date_scheduled,e.schedule_type,
		e.event_duration,me.experience_name,me.cause,
        	e.address,s.state_name,c.city_name
						FROM ltg_requested_opportunities as rq
                          	JOIN user as u ON 
							rq.user_id = u.id
                                JOIN events as e ON 
                                rq.event_id = e.id
                                	JOIN master_experience as me ON 
									e.experience_id = me.id
                                        JOIN states as s ON 
										e.state_id = s.id 
											JOIN cities as c ON 
											e.city_id = c.id
                                            order by rq.created_on desc";
        $command = Yii::$app->db->createCommand($opp_list);
        $opp_list_result = $command->queryAll();

        // print_r($events);exit;

        if (empty($opp_list_result)) {
            return $result = ['status' => 'fail', 'message' => 'No data'];
        }

        $array = [];
        $i = 1;
        foreach ($opp_list_result as $opp_res) {
            $out = [
                'index' => $i,
                'req_opp_id' => $opp_res['request_id'],
                'user_name' => $opp_res['first_name'] . ' ' . $opp_res['last_name'],
                'email' => $opp_res['email'],
                'phone' => $opp_res['phone'],
                'experience_name' => $opp_res['experience_name'],
                'schedule_type' => $opp_res['schedule_type'],
                'organisation' => $opp_res['organisation'],
                'request_date' => date('d F Y', strtotime($opp_res['request_date'])),
                'req_no_of_volunteers' => $opp_res['no_of_volunteers'],
                'description' => $opp_res['description'],
                'address' => $opp_res['address'],
                'state_name' => $opp_res['state_name'],
                'city_name' => $opp_res['city_name'],
                'status_flag' => $opp_res['status_flag'],
                'event_info' => $opp_res['event_info']
            ];

            array_push($array, $out);
            $i++;
        }

        return $result = ['content' => $array, 'status' => 'success', 'message' => 'list of requested opportunity'];
    }

    public function actionApprove_custom_requests() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);
        $id = $request['id'];
        $requestedoppurtunity = LtgRequestedOpportunities::find()->where(['id' => $id])->one();
        if (empty($requestedoppurtunity)) {
            return $result = ['status' => 'fail', 'message' => 'Request not found'];
        }
        //Checking whether the opportunity has already been approved
        $checkstatus = LtgRequestedOpportunities::find()->select(['status_flag'])->where(['id' => $id, 'status_flag' => 1])->scalar();
        if (!empty($checkstatus)) {
            return $result = ['status' => 'fail', 'message' => 'Already approved'];
        }
        $requestedoppurtunity->status_flag = 1;
        $requestedoppurtunity->save(false);

        return $result = ['status' => 'success', 'message' => 'Approved'];
    }

    public function actionCitylist() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);

        $cities = Cities::find()->select(['id', 'city_name'])->all();
        if (empty($cities)) {
            return $result = ['status' => 'fail', 'message' => 'City not found'];
        }

        return $result = ['data' => $cities, 'status' => 'success', 'message' => 'List of cities'];
    }

    public function actionStatelist() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);

        $states = States::find()->select(['id', 'state_name', 'state_code', 'IsActive', 'country_id'])->orderBy('id DESC')->all();
        if (empty($states)) {
            return $result = ['status' => 'fail', 'message' => 'City not found'];
        }

        $array = [];
        $i = 1;
        foreach ($states as $states) {
            $country = Country::find()->select(['country_name'])->where(['id' => $states->country_id])->scalar();
            $out = [
                'index' => $i,
                'id' => $states['id'],
                'state_name' => $states['state_name'],
                'state_code' => $states['state_code'],
                'country_name' => $country,
                'IsActive' => $states['IsActive']
            ];

            array_push($array, $out);
            $i++;
        }

        return $result = ['data' => $array, 'status' => 'success', 'message' => 'List of states'];
    }

    public function actionStatelist_active() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);

        // $states = States::find()->select(['id','state_name','state_code','IsActive'])->all();
        $states = States::find()->where("`IsActive`" . " ='1'")->all();


        if (empty($states)) {
            return $result = ['status' => 'fail', 'message' => 'Not states found'];
        }

        $array = [];
        $i = 1;
        foreach ($states as $states) {
            $country = Country::find()->select(['country_name'])->where(['id' => $states->country_id])->scalar();
            $out = [
                'index' => $i,
                'id' => $states['id'],
                'state_name' => $states['state_name'],
                'state_code' => $states['state_code'],
                'country_name' => $country,
                'IsActive' => $states['IsActive']
            ];

            array_push($array, $out);
            $i++;
        }

        return $result = ['data' => $array, 'status' => 'success', 'message' => 'List of states'];
    }

    public function actionInsert_state() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);

        $state_name = $request['state_name'];
        $state_code = $request['state_code'];
        $status = '1';
        $country_id = $request['country_id'];
        $states = States::find()->where(['state_name' => $state_name])->one();
        if (empty($states)) {
            $model = new States();  // Storing new model record in a variable

            $model->state_name = $state_name;
            $model->state_code = $state_code;
            $model->IsActive = $status;
            $model->country_id = $country_id;
            $model->save(false); // To exceute the query

            return $result = ['status' => "success", 'message' => 'State Inserted Succesfull'];
        } else {

            return $result = ['status' => "fail", 'message' => 'Already State name exists'];
        }
    }

    public function actionGetstatelist() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);
        $state_id = $request['state_id'];

        $states = States::find()->where("`id`" . " ='" . $state_id . "'")->one();
        if (empty($states)) {
            return $result = ['status' => 'fail', 'message' => 'State not found'];
        }


        $out = [
            'id' => $states->id,
            'state_name' => $states->state_name,
            'state_code' => $states->state_code,
            'country_id' => $states->country_id,
            'IsActive' => $states->IsActive
        ];



        return $result = ['data' => $out, 'status' => 'success', 'message' => 'state details'];
    }

    /* To edit Events */

    public function actionEdit_state() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);
        $country_id = $request['country_id'];
        $state_name = $request['state_name'];
        $state_code = $request['state_code'];
        $IsActive = $request['IsActive'];


        $state_id = $request['id'];

        $model = States::find()->where(['id' => $state_id])->one();
        if (empty($model)) {
            return $result = ['status' => 'fail', 'message' => 'state not exists'];
        }


        $model->state_name = $state_name;
        $model->state_code = $state_code;
        $model->IsActive = $IsActive;
        $model->country_id = $country_id;
        $model->save(false);
        return $result = ['status' => 'success', 'message' => 'State Updated Successfully'];
    }

    public function actionDelete_state() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);

        $state_id = $request['id'];

        $model = States::find()->where(['id' => $state_id])->one();
        if (empty($model)) {
            return $result = ['status' => 'fail', 'message' => 'State not exists'];
        }
        $model->delete();

        return $result = ['status' => 'success', 'message' => 'State Deleted Successfully'];
    }

    /*     * * City Module starts here ** */

    public function actionCity_list_admin() {
        $citylist = "SELECT  c.id,c.city_name,c.IsActive,s.state_name,s.state_code,country.country_name
						FROM cities as c
                          JOIN states as s ON 
							c.state_id = s.id 
							JOIN country as country ON 
							country.id = s.country_id 
								order by id desc";

        $command = Yii::$app->db->createCommand($citylist);
        $cities = $command->queryAll();
        // print_r($events);exit;

        if (empty($cities)) {
            return $result = ['status' => 'fail', 'message' => 'No data'];
        }

        $array = [];

        $i = 1;
        foreach ($cities as $city) {

            $out = [
                'index' => $i,
                'id' => $city['id'],
                'city_name' => $city['city_name'],
                'state_name' => $city['state_name'],
                'state_code' => $city['state_code'],
                'country_name' => $city['country_name'],
                'status' => $city['IsActive']
            ];

            array_push($array, $out);
            $i++;
        }

        return $result = ['content' => $array, 'status' => 'success', 'message' => 'list of cities'];
    }

    public function actionInsert_city() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);

        $city_name = $request['city_name'];
        $state_id = $request['state_id'];
        $status = '1';

        $states = States::find()->where(['id' => $state_id])->one();

        if (empty($states)) {
            return $result = ['status' => "fail", 'message' => 'No such state exists'];
        }

        $cities = Cities::find()->where(['city_name' => $city_name])->one();
        if (empty($cities)) {
            $model = new Cities();  // Storing new model record in a variable

            $model->city_name = $city_name;
            $model->state_id = $state_id;
            $model->IsActive = $status;
            $model->save(false); // To exceute the query

            return $result = ['status' => "success", 'message' => 'City Inserted Succesfull'];
        } else {

            return $result = ['status' => "fail", 'message' => 'Already city name exists'];
        }
    }

    public function actionGetcitydetails() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);

        $city_id = $request['city_id'];

        $cities = Cities::find()->where("`id`" . " ='" . $city_id . "'")->one();
        if (empty($cities)) {
            return $result = ['status' => 'fail', 'message' => 'city not found'];
        }
        $country = States::find()->select(['country_id'])->where(['id' => $cities->state_id])->scalar();
        $out = [
            'id' => $cities->id,
            'city_name' => $cities->city_name,
            'state_id' => $cities->state_id,
            'IsActive' => $cities->IsActive,
            'country_id' => $country
        ];

        return $result = ['data' => $out, 'status' => 'success', 'message' => 'city details'];


        return $result = ['id' => $id, 'city_name' => $city_name, 'state_id' => $state_id, 'status' => $status, 'status' => 'success', 'message' => 'city details'];
    }

    public function actionGetcity_wrtstate() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);

        $state_id = $request['state_id'];

        $cities = Cities::find()->where(['state_id' => $state_id, 'IsActive' => '1'])->all();
        if (empty($cities)) {
            return $result = ['status' => 'fail', 'message' => 'city not found'];
        }

        $array = [];

        foreach ($cities as $city) {

            $out = [
                'id' => $city['id'],
                'city_name' => $city['city_name']
            ];

            array_push($array, $out);
        }

        return $result = ['content' => $array, 'status' => 'success', 'message' => 'list of cities'];
    }

    public function actionDelete_city() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);

        $city_id = $request['id'];

        $model = Cities::find()->where(['id' => $city_id])->one();
        if (empty($model)) {
            return $result = ['status' => 'fail', 'message' => 'City not exists'];
        }
        $model->delete();

        return $result = ['status' => 'success', 'message' => 'City Deleted Successfully'];
    }

    public function actionEdit_city() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);

        $id = $request['id'];
        $state_id = $request['state_id'];
        $city_name = $request['city_name'];
        $IsActive = $request['IsActive'];
        $country_id = $request['country_id'];
        $states = States::find()->where(['id' => $state_id])->one();


        if (empty($states)) {
            return $result = ['status' => "fail", 'message' => 'No such state exists'];
        }


        $model = Cities::find()->where(['id' => $id])->one();
        if (empty($model)) {
            return $result = ['status' => 'fail', 'message' => 'city not exists'];
        }


        $model->state_id = $state_id;
        $model->city_name = $city_name;
        $model->IsActive = $IsActive;

        $model->save(false);
        $states->country_id = $country_id;
        $states->save(false);
        return $result = ['status' => 'success', 'message' => 'City name Updated Successfully'];
    }

    public function actionRequested_opportunity_details() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);

        $request_id = $request['request_id'];



        $opp_list = "SELECT rq.id as request_id,rq.created_on,rq.status_flag,rq.organisation,rq.request_date,rq.no_of_volunteers,rq.description,u.first_name,u.last_name,u.email,u.phone,
		e.id as event_id,e.experience_id,e.date_scheduled,e.schedule_type,
		e.event_duration,me.experience_name,me.cause,
        	e.address,s.state_name,c.city_name
						FROM ltg_requested_opportunities as rq
                          	JOIN user as u ON 
							rq.user_id = u.id
                                JOIN events as e ON 
                                rq.event_id = e.id
                                	JOIN master_experience as me ON 
									e.experience_id = me.id
                                        JOIN states as s ON 
										e.state_id = s.id 
											JOIN cities as c ON 
											e.city_id = c.id
											where rq.id = $request_id";
        $command = Yii::$app->db->createCommand($opp_list);
        $opp_list_result = $command->queryAll();

        // print_r($events);exit;

        if (empty($opp_list_result)) {
            return $result = ['status' => 'fail', 'message' => 'No data'];
        }



        $opp_res = $opp_list_result[0];

        $request_id = $opp_res['request_id'];
        $user_name = $opp_res['first_name'] . ' ' . $opp_res['last_name'];
        $experience_name = $opp_res['experience_name'];
        $schedule_type = $opp_res['schedule_type'];
        $organisation = $opp_res['organisation'];
        $no_of_volunteers = $opp_res['no_of_volunteers'];
        $request_date = date('d F Y H:i', strtotime($opp_res['request_date']));
        $description = $opp_res['description'];
        $no_of_volunteers = $opp_res['no_of_volunteers'];
        $address = $opp_res['address'];
        $state_name = $opp_res['state_name'];
        $city_name = $opp_res['city_name'];
        $status = $opp_res['status_flag'];
        $mobile = $opp_res['phone'];


        return $result = ['request_id' => $request_id, 'experience_name' => $experience_name, 'user_name' => $user_name, 'schedule_type' => $schedule_type, 'organisation' => $organisation, 'request_date' => $request_date, 'no_of_volunteers' => $no_of_volunteers, 'description' => $description, 'address' => $address, 'state_name' => $state_name, 'city_name' => $city_name, 'phone' => $mobile, 'request_status' => $status, 'status' => 'success', 'message' => 'requested opp details'];

        return $result = ['content' => $array, 'status' => 'success', 'message' => 'list of requested opportunity'];
    }

    public function actionDashboard_admin() {

        $pre_schedule_count = Events::find()->where(['schedule_type' => 'Pre-Schedule'])->count();
        $custom_schedule_count = Events::find()->where(['schedule_type' => 'Custom-Schedule'])->count();
        $volunteers_count = User::find()->where(['role_type' => '2'])->count();
        // $custom_schedule_count = Events::find()->where(['schedule_type'=>'Pre-Schedule','attendance_flag'=>2])->count();
        $image_path = "http://staging.api.letstagon.com/profile_images/";
        $skills = MasterExperience::find()->where(['category' => 'Skill Based Volunteering'])->count();
        $sensitization = MasterExperience::find()->where(['category' => 'Sensitization workshops'])->count();
        $group = MasterExperience::find()->where(['category' => 'Group Volunteering Activities'])->count();
        $individual = MasterExperience::find()->where(['category' => 'Individual Volunteering Activities'])->count();
        $users = User::find()->select(['first_name', 'last_name', 'image_path', 'email'])->orderBy(['id' => SORT_DESC])->limit(4)->all();
        $array = [];
        foreach ($users as $user) {

            $out = [
                'firstname' => $user->first_name,
                'lastname' => $user->last_name,
                'image_path' => $image_path . $user->image_path,
                'email' => $user->email
            ];
            array_push($array, $out);
        }

     

        $out = [
            'pre_schedule_count' => $pre_schedule_count,
            'custom_schedule_count' => $custom_schedule_count,
            'total_volunteers_count' => $volunteers_count,
            'total_events_count' => $custom_schedule_count + $pre_schedule_count,
            'skill_based_volunteering' => $skills,
            'sensitization_workshops' => $sensitization,
            'group_volunteering' => $group,
            'individual_volunteering' => $individual,
            'user' => $array
        ];
        return $result = ['data' => $out, 'status' => 'success', 'message' => 'Dashboard Count'];
    }

    public function actionCounters() {

        $events_count = Events::find()->count();
        $volunteers_count = User::find()->where(['role_type' => '2'])->count();
        $group_events = MasterNgo::find()->count();
        $eventhours = Events::find()->sum('event_duration');
        $out = [
            'event_count' => $events_count,
            'volunteer_count' => $volunteers_count,
            'group_count' => $group_events,
            'event_hours' => $eventhours
        ];
        return $result = ['data' => $out, 'status' => 'success', 'message' => 'counters'];
    }

    public function actionDeletevolunteer() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);

        $userid = $request['user_id'];

        $volunteers = User::find()->where(['id' => $userid])->one();
        if (empty($volunteers)) {
            return $result = ['status' => 'fail', 'user not found'];
        }
        $volunteers->delete();

        return $result = ['status' => 'success', 'message' => 'Deleted Sucessfully'];
    }

    public function actionCountry_list() {

        $countrylist = Country::find()->select(['id', 'country_name', 'country_code', 'IsActive'])->orderBy('id desc')->all();

        if (empty($countrylist)) {
            return $result = ['status' => 'fail', 'message' => 'No country list'];
        }
        $array = [];
        $i = 1;
        foreach ($countrylist as $country) {
            $out = [
                'index' => $i,
                'id' => $country->id,
                'country_name' => $country->country_name,
                'country_code' => $country->country_code,
                'Isactive' => $country->IsActive,
            ];
            $i++;
            array_push($array, $out);
        }
        return $result = ['data' => $array, 'status' => 'success', 'message' => 'Country list'];
    }

    public function actionCountry_list_active() {

        $countrylist = Country::find()->select(['id', 'country_name', 'country_code', 'IsActive'])->where(['IsActive' => 0])->all();

        if (empty($countrylist)) {
            return $result = ['status' => 'fail', 'message' => 'No country list'];
        }
        $array = [];
        $i = 1;
        foreach ($countrylist as $country) {
            $out = [
                'index' => $i,
                'id' => $country->id,
                'country_name' => $country->country_name,
                'country_code' => $country->country_code,
                'Isactive' => $country->IsActive,
            ];
            $i++;
            array_push($array, $out);
        }
        return $result = ['data' => $array, 'status' => 'success', 'message' => 'Country list'];
    }

    public function actionInsert_country() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);
        $country_name = $request['country_name'];
        $country_code = $request['country_code'];
        $country = Country::find()->where(['country_code' => $country_code])->one();
        if (!empty($country)) {
            return $result = ['status' => 'fail', 'message' => 'Country code already exists'];
        }
        $model = new Country();
        $model->country_name = $country_name;
        $model->country_code = $country_code;
        $model->save(false);
        return $result = ['status' => 'success', 'message' => 'Country added successfully'];
    }

    public function actionEdit_country() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);
        $country_name = $request['country_name'];
        $country_code = $request['country_code'];
        $id = $request['id'];
        $isactive = $request['IsActive'];
        $model = Country::find()->where(['id' => $id])->one();
        if (empty($model)) {
            return $result = ['status' => 'fail', 'message' => 'No data'];
        }
        $model->country_name = $country_name;
        $model->country_code = $country_code;
        $model->IsActive = $isactive;
        $model->save(false);
        return $result = ['status' => 'success', 'message' => 'Country updated successfully'];
    }

    public function actionDelete_country() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);
        $id = $request['id'];
        $model = Country::find()->where(['id' => $id])->one();
        if (empty($model)) {
            return $result = ['status' => 'fail', 'message' => 'No data'];
        }
        $model->delete();
        return $result = ['status' => 'success', 'message' => 'Country deleted successfully'];
    }

    public function actionCreate_ngo() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);
        $name = $request['ngo_name'];
        $type = $request['ngo_type'];
        $description = $request['ngo_description'];
        $contact_person = $request['contact_person'];
        $contact_number = $request['contact_number'];
        $email = $request['email'];
        $address = $request['address'];
        $country = $request['country'];
        $state = $request['state'];
        $city = $request['city'];

        $model_name = MasterNgo::find()->where(['ngo_name' => $name])->one();

        if (!empty($model_name)) {
            return $result = ['status' => "fail", 'message' => 'The organization is already registered. Please write to us at info@letstagon.com for specific requirements.'];
        }


        $model = new MasterNgo();
        $model->ngo_name = $name;
        $model->ngo_type = $type;
        $model->description = $description;
        $model->contact_person = $contact_person;
        $model->contact_number = $contact_number;
        $model->email = $email;
        $model->address = $address;
        $model->country_id = $country;
        $model->state_id = $state;
        $model->city_id = $city;
        $model->save(false);
        return $result = ['status' => 'success', 'message' => 'Thank you for registering with us. We will get in touch soon.'];
    }

    public function actionNgo_list() {

       $ngolist = MasterNgo::find()->select(['id', 'ngo_name', 'ngo_type', 'description', 'email', 'contact_person', 'contact_number', 'country_id', 'state_id', 'city_id'])->orderBy ('id desc')->all();
      
        if (empty($ngolist)) {
            return $result = ['status' => 'fail', 'message' => 'No ngo list'];k;
        }
        $i = 1;
        $array = [];
        foreach ($ngolist as $ngo) {
            $out = [
                'index' => $i,
                'id' => $ngo['id'],
                'ngo_name' => $ngo['ngo_name'],
                'ngo_type' => $ngo['ngo_type'],
                'description' => $ngo['description'],
                'email' => $ngo['email'],
                'contact_person' => $ngo['contact_person'],
                'contact_number' => $ngo['contact_number'],
                'country_id' => $ngo['country_id'],
                'state_id' => $ngo['state_id'],
                'city_id' => $ngo['city_id']
            ];
            array_push($array, $out);
            $i++;
        }
        return $result = ['data' => $array, 'status' => 'success', 'message' => 'Ngo list'];
    }

    public function actionEdit_ngo() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);
        $id = $request['id'];
        $name = $request['ngo_name'];
        $type = $request['ngo_type'];
        $description = $request['description'];
        $contact_person = $request['contact_person'];
        $contact_number = $request['contact_number'];
        $email = $request['email'];
        $address = $request['address'];
        $country = $request['country_id'];
        $state = $request['state_id'];
        $city = $request['city_id'];



        $model = MasterNgo::find()->where(['id' => $id])->one();
        if (empty($model)) {
            return $result = ['status' => 'fail', 'message' => 'Ngo id does not exists'];
        }

        $model->ngo_name = $name;
        $model->ngo_type = $type;
        $model->description = $description;
        $model->contact_person = $contact_person;
        $model->contact_number = $contact_number;
        $model->email = $email;
        $model->address = $address;
        $model->country_id = $country;
        $model->state_id = $state;
        $model->city_id = $city;
        $model->save(false);

        return $result = ['status' => 'success', 'message' => 'Ngo updated successfully'];
    }

    public function actionDelete_ngo() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);
        $id = $request['id'];
        $model = MasterNgo::find()->where(['id' => $id])->one();
        if (empty($model)) {
            return $result = ['status' => 'fail', 'message' => 'Ngo id does not exists'];
        }
        $model->delete();
        return $result = ['status' => 'success', 'message' => 'Ngo deleted successfully'];
    }

   public function actionEvent_locations() 
   {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);
        $user_id=$request['user_id'];
        $experience_id = $request['experience_id'];
        $eventlocations = "SELECT e.id,e.address,e.date_scheduled,c.city_name from events as e 
		JOIN cities as c ON e.city_id = c.id
		where e.experience_id = '" . $experience_id . "' and e.event_info='public' and e.date_scheduled >= CURDATE() and e.event_info='public'";
        $events = Yii::$app->db->createCommand($eventlocations);
        $eventlists = $events->queryAll();
		
        if (empty($eventlists)) 
        {
            return $result = ['status' => 'fail', 'message' => 'No data'];
        }

        $array = [];
        foreach ($eventlists as $event) 
		{ 
		
            $events=$event['id'];
            $out = [
                'id' => $event['id'],
                'location' => $event['address']." ".$event['city_name'],
                'date_scheduled' => $event['date_scheduled']
            ];
           $query= UserTaggedExperiences::find()->where(['user_id'=>$user_id,'event_id'=>$events])->one();
		  
		    if(empty($query))
          {
            array_push($array, $out);
			
          }
		  
		}
		
          if(empty($array))
		  {
			  return $result = [ 'status' => 'fail', 'message' => 'No locations'];
		  }
		  else
		  {
          return $result = ['data' => $array, 'status' => 'success', 'message' => 'event locations'];
       
		  } 
     }

    public function actionView_ngo() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);
        $id = $request['id'];
        $ngo = "SELECT ngo.ngo_name,ngo.ngo_type,ngo.description,ngo.email,ngo.contact_number,ngo.contact_person,ngo.address,country.country_name,state.state_name,city.city_name
						FROM master_ngo as ngo
						JOIN country as country ON 
										ngo.country_id = country.id 
                                        JOIN states as state ON 
										ngo.state_id = state.id 
											JOIN cities as city ON 
											ngo.city_id = city.id
											where ngo.id = '" . $id . "'";
        $command = Yii::$app->db->createCommand($ngo);
        $ngodata = $command->queryAll();

        // print_r($events);exit;

        if (empty($ngodata)) {
            return $result = ['status' => 'fail', 'message' => 'No data'];
        }

        $ngo = $ngodata[0];
        $out = [
            'ngo_name' => $ngo['ngo_name'],
            'ngo_type' => $ngo['ngo_type'],
            'description' => $ngo['description'],
            'email' => $ngo['email'],
            'contact_number' => $ngo['contact_number'],
            'contact_person' => $ngo['contact_person'],
            'address' => $ngo['address'],
            'country_name' => $ngo['country_name'],
            'state_name' => $ngo['state_name'],
            'city_name' => $ngo['city_name'],
        ];



        return $result = ['data' => $out, 'status' => 'success', 'message' => 'Ngo details'];
    }

    public function actionView_ngo_id() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);
        $id = $request['id'];
        $ngodata = MasterNgo::find()->select(['id', 'ngo_name', 'ngo_type', 'description', 'email', 'contact_person', 'contact_number', 'address', 'country_id', 'state_id', 'city_id'])->where(['id' => $id])->one();
        return $result = ['data' => $ngodata, 'status' => 'success', 'message' => 'Ngo details'];
    }

    public function actionCountry_by_id() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);
        $id = $request['id'];
        $country = Country::find()->select(['id', 'country_name', 'country_code', 'IsActive'])->where(['id' => $id])->one();
        if (empty($country)) {
            return $result = ['status' => 'fail', 'message' => 'No data'];
        }

        return $result = ['data' => $country, 'status' => 'success', 'message' => 'Ngo details'];
    }

    public function actionStates_by_country() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);
        $country_id = $request['country_id'];
        $states = States::find()->select(['id', 'state_name'])->where(['country_id' => $country_id, 'IsActive' => 1])->all();
        if (empty($states)) {
            return $result = ['status' => 'fail', 'message' => 'No data'];
        }

        return $result = ['data' => $states, 'status' => 'success', 'message' => 'State details'];
    }

    public function actionInsert_private_events() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);
        $event_type = $request['event_type'];
        $email = $request['email'];
        $event_name = $request['event_name'];
        $schedule_type = $request['schedule_type'];
        $participation_type = $request['participation_type'];
        $mode = $request['mode'];
        $suitable_for = $request['suitable_for'];
        $no_of_volunteers = $request['no_of_volunteers'];
        $date_scheduled = $request['date_scheduled'];
        $event_duration = $request['event_duration'];
        $state_id = $request['state_id'];
        $city_id = $request['city_id'];
        $address = $request['address'];
        $country_id = $request['country_id'];
        $experience_id = $request['experience_id'];
        $event_info = $request['event_info'];
        $status = '1';

        $datee = date('Y-m-d H:i', strtotime($date_scheduled));

        $experience_identity = MasterExperience::find()->where(['id' => $experience_id])->one();
        
        if (empty($experience_identity)) {
            return $result = ['status' => "fail", 'message' => 'No such experience exists'];
        }

        $model = new Events();  // Storing new model record in a variable

        $model->event_type = $event_type;
        $model->schedule_type = $schedule_type;
        $model->participation_type = $participation_type;
        $model->mode = $mode;
        $model->suitable_for = $suitable_for;
        $model->no_of_volunteers = $no_of_volunteers;
        $model->date_scheduled = $datee;
        $model->event_duration = $event_duration;
        $model->state_id = $state_id;
        $model->city_id = $city_id;
        $model->address = $address;
        $model->experience_id = $experience_id;
        $model->country_id = $country_id;
        $model->status = $status;
        $model->event_info = $event_info;
        $model->save(false); // To exceute the query
        $id = $model->id;
       
        $experience=Events::find()->select(['experience_id'])->where(['id'=>$id])->one();
		$exp_id=$experience->experience_id;
      
        $message = "<table border='1' cellpadding='10' cellspacing='10' width='100%' height='100'>
				<tr>
				<td><img class='logo_light' alt='Letstagon Logo' src='http://agathsya.com/letstagon/admin/Admin/assets/images/logo-icon.png'><img class='logo_light' alt='Letstagon Logo' src='http://agathsya.com/letstagon/admin/Admin/assets/images/logo-text.png'></td>
				</tr>
				<tr>
				<td width='100%' height='60'>
				<p></p>
				<p></p>";
        $message .= 'Greetings ' . $email . ',<br><br>';
        $message .= '<p>click on the below link  to create an event</p>';
        $message .= '<p><a style="font-size:16px;font-family:Helvetica,Helvetica neue,Arial,Verdana,sans-serif;font-weight:none;color:#ffffff;text-decoration:none;background-color:#3572b0;border-top:11px solid #3572b0;border-bottom:11px solid #3572b0;border-left:20px solid #3572b0;border-right:20px solid #3572b0;border-radius:5px;display:inline-block" class="m_1789912164564125250mobile-button" href="http://staging.letstagon.com/#/detail-description/'.$exp_id.'" target="_blank" data-saferedirecturl="">click here</a></p>';
        $message .= '<p><b>Happy volunteering,</b></p>';
        $message .= '<p><b>LetsTagOn team</b></p>';
        $message .= "
				</td>
				</tr>
				<tr>
				<td width='100%' cellpadding='1' cellspacing='1' bgcolor='#DFDFDF' height='1'>
				</td>
				</tr>
			        </table>";
        Yii::$app->mailer->compose()
                ->setFrom(['support@letstagon.com'=>'LetsTagOn'])
                ->setTo($email)
                ->setSubject('Private oppourtunity-LetsTagOn')
                ->setHtmlBody($message)
                ->send();
        return $result = ['status' => "success", 'message' => 'private event created and mail has sent to particular user'];
    }
    
    public function actionCreate_ngo_admin() {
        $post = file_get_contents("php://input");
        $request = Json::decode($post);
        $name = $request['ngo_name'];
        $type = $request['ngo_type'];
        $description = $request['ngo_description'];
        $contact_person = $request['contact_person'];
        $contact_number = $request['contact_number'];
        $email = $request['email'];
        $address = $request['address'];
        $country = $request['country'];
        $state = $request['state'];
        $city = $request['city'];
        
        $model_name = MasterNgo::find()->where(['ngo_name' => $name])->one();

        if (!empty($model_name)) {
            return $result = ['status' => "fail", 'message' => 'The organization is already registered. Please write to us at info@letstagon.com for specific requirements.'];
        }


        $model = new MasterNgo();
        $model->ngo_name = $name;
        $model->ngo_type = $type;
        $model->description = $description;
        $model->contact_person = $contact_person;
        $model->contact_number = $contact_number;
        $model->email = $email;
        $model->address = $address;
        $model->country_id = $country;
        $model->state_id = $state;
        $model->city_id = $city;
        $model->save(false);
        
        $ngo_name= $model->ngo_name;
        $contact_person=$model->contact_person;
        $email=$model->email;
        $address=$model->address;
      
       
        $message = "<table border='1' cellpadding='10' cellspacing='10' width='100%' height='100'>
				<tr>
				<td><img class='logo_light' alt='Letstagon Logo' src='http://agathsya.com/letstagon/admin/Admin/assets/images/logo-icon.png'><img class='logo_light' alt='Letstagon Logo' src='http://agathsya.com/letstagon/admin/Admin/assets/images/logo-text.png'></td>
				</tr>
				<tr>
				<td width='100%' height='60'>
				<p></p>
				<p></p>";
        $message .= 'Greetings Admin<br><br>';
        $message .= '<p>'.$ngo_name.'  NGO has created successfully by  '.$contact_person.'  with the Email-id  '.$email.'  in  '.$address.' </p>';
        $message .= '<p><b>Happy volunteering,</b></p>';
        $message .= '<p><b>LetsTagOn team</b></p>';
        $message .= "
				</td>
				</tr>
				<tr>
				<td width='100%' cellpadding='1' cellspacing='1' bgcolor='#DFDFDF' height='1'>
				</td>
				</tr>
			        </table>";
        Yii::$app->mailer->compose()
                ->setFrom(['support@letstagon.com'=>'LetsTagOn'])
                ->setTo('info@letstagon.net')
                ->setSubject('NGO CREATION-LetsTagOn')
                ->setHtmlBody($message)
                ->send();
        return $result = ['status' => "success", 'message' => 'Thank you for registering with us. We will get in touch soon.'];
    
    }

}
