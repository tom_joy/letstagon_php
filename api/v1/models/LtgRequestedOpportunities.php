<?php

namespace api\v1\models;

use Yii;

/**
 * This is the model class for table "ltg_requested_opportunities".
 *
 * @property int $id
 * @property int $event_id
 * @property int $user_id
 * @property string $organisation
 * @property string $request_date
 * @property int $no_of_volunteers
 * @property string $description
 * @property string $address
 * @property int $status_flag
 * @property int $IsActive
 * @property string $created_on
 * @property string $updated_on
 *
 * @property Events $event
 * @property User $user
 */
class LtgRequestedOpportunities extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ltg_requested_opportunities';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['event_id', 'user_id', 'organisation', 'request_date', 'no_of_volunteers', 'description', 'address', 'IsActive'], 'required'],
            [['event_id', 'user_id', 'no_of_volunteers', 'status_flag', 'IsActive'], 'integer'],
            [['description', 'address'], 'string'],
            [['created_on', 'updated_on'], 'safe'],
            [['organisation', 'request_date'], 'string', 'max' => 255],
            [['event_id'], 'exist', 'skipOnError' => true, 'targetClass' => Events::className(), 'targetAttribute' => ['event_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_id' => 'Event ID',
            'user_id' => 'User ID',
            'organisation' => 'Organisation',
            'request_date' => 'Request Date',
            'no_of_volunteers' => 'No Of Volunteers',
            'description' => 'Description',
            'address' => 'Address',
            'status_flag' => 'Status Flag',
            'IsActive' => 'Is Active',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Events::className(), ['id' => 'event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
