<?php

namespace api\v1\models;

use Yii;

/**
 * This is the model class for table "events".
 *
 * @property int $id
 * @property string $event_type
 * @property string $schedule_type
 * @property string $participation_type
 * @property int $experience_id
 * @property string $mode
 * @property string $suitable_for
 * @property int $no_of_volunteers
 * @property string $date_scheduled
 * @property int $event_duration
 * @property string $event_info
 * @property int $country_id
 * @property int $state_id
 * @property int $city_id
 * @property string $address
 * @property double $lat
 * @property double $lng
 * @property int $status 0-Inactive,1-Active
 * @property int $IsActive
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 *
 * @property MasterExperience $experience
 * @property States $state
 * @property Cities $city
 * @property Country $country
 * @property LtgRequestedOpportunities[] $ltgRequestedOpportunities
 * @property Reviews[] $reviews
 * @property UserTaggedExperiences[] $userTaggedExperiences
 */
class Events extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'events';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['event_type', 'schedule_type', 'participation_type', 'experience_id', 'mode', 'suitable_for', 'no_of_volunteers', 'date_scheduled', 'event_duration', 'country_id', 'state_id', 'city_id', 'address', 'lat', 'lng', 'status'], 'required'],
            [['experience_id', 'no_of_volunteers', 'event_duration', 'country_id', 'state_id', 'city_id', 'status', 'IsActive', 'created_by', 'updated_by'], 'integer'],
            [['date_scheduled', 'created_at', 'updated_at'], 'safe'],
            [['address'], 'string'],
            [['lat', 'lng'], 'number'],
            [['event_type', 'schedule_type', 'participation_type', 'mode', 'suitable_for', 'event_info'], 'string', 'max' => 255],
            [['experience_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterExperience::className(), 'targetAttribute' => ['experience_id' => 'id']],
            [['state_id'], 'exist', 'skipOnError' => true, 'targetClass' => States::className(), 'targetAttribute' => ['state_id' => 'id']],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_type' => 'Event Type',
            'schedule_type' => 'Schedule Type',
            'participation_type' => 'Participation Type',
            'experience_id' => 'Experience ID',
            'mode' => 'Mode',
            'suitable_for' => 'Suitable For',
            'no_of_volunteers' => 'No Of Volunteers',
            'date_scheduled' => 'Date Scheduled',
            'event_duration' => 'Event Duration',
            'event_info' => 'Event Info',
            'country_id' => 'Country ID',
            'state_id' => 'State ID',
            'city_id' => 'City ID',
            'address' => 'Address',
            'lat' => 'Lat',
            'lng' => 'Lng',
            'status' => 'Status',
            'IsActive' => 'Is Active',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExperience()
    {
        return $this->hasOne(MasterExperience::className(), ['id' => 'experience_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(States::className(), ['id' => 'state_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLtgRequestedOpportunities()
    {
        return $this->hasMany(LtgRequestedOpportunities::className(), ['event_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(Reviews::className(), ['event_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserTaggedExperiences()
    {
        return $this->hasMany(UserTaggedExperiences::className(), ['event_id' => 'id']);
    }
}
