<?php

namespace api\v1\models;

use Yii;

/**
 * This is the model class for table "states".
 *
 * @property int $id
 * @property string $state_name
 * @property string $state_code
 * @property int $IsActive
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 *
 * @property Address[] $addresses
 * @property Cities[] $cities
 * @property ExperiencesLocation[] $experiencesLocations
 */
class States extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'states';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['state_name', 'state_code', 'IsActive'], 'required'],
            [['IsActive', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['state_name'], 'string', 'max' => 255],
            [['state_code'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'state_name' => 'State Name',
            'state_code' => 'State Code',
            'IsActive' => 'Is Active',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(Address::className(), ['state_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(Cities::className(), ['state_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExperiencesLocations()
    {
        return $this->hasMany(ExperiencesLocation::className(), ['state_id' => 'id']);
    }
}
