<?php

namespace api\v1\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\db\Query;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\helpers\Json;
use yii\db\Command;
use yii\web\UploadedFile;
use api\v1\models\OutsideOppourtunity;
use api\v1\models\User;
use api\v1\models\LoginForm;
use api\v1\models\Events;
use api\v1\models\LtgRequestedOpportunities;
use api\v1\models\ExperienceCategory;
use api\v1\models\Testimonials;
use api\v1\models\Reviews;
use api\v1\models\UserTaggedExperiences;
class UserController extends ActiveController {

    public $modelClass = 'api\v1\models\User';  // Define a model class name for the controller

public function behaviors()
{
    return [
        'corsFilter' => [
            'class' => \yii\filters\Cors::className(),
        ],
    ];
}
/* API For User Registration */

public function actionRegister()
{
	 $post = file_get_contents("php://input");
     $request = Json::decode($post);
	 
	 $first_name 	= $request['first_name'];
	 $last_name 	= "";
	 $email 		= $request['email'];
	 $password 		= $request['password'];
	 $phone 		= $request['phone'];
	 $dob 			= "";
	 $designation 	= "";
	 $organization 	= ""; 
	 $role_type 	= '2';
	 $status 		= '0';
	 $user_name 	= $first_name .' '.$last_name;
	 
	 $identity =  User::find()->where("`email`" . " ='" . $email ."'")->one();
	 
	 $mobile = User::find()->where(['phone'=>$phone])->one();

	 if(!empty($mobile)){
	 	return $result = ['status' => "fail", 'message' => 'Mobile number already exists'];
	 }
	 if(empty($identity)){
		 
		 $user = new User();  // Storing new model record in a variable
		 $user->first_name		=	$first_name;
		 $user->last_name		=	$last_name;
		 $user->email			=	$email;
		 $user->phone			=	$phone;
		 $user->dob				=	$dob;
		 $user->designation		=	$designation;
		 $user->organization	=	$organization;
		 $user->role_type		=	$role_type;
		 $user->status			=	$status;
		 $user->image_path		=   "default_icon.jpg";
		
		 $user->password_hash = Yii::$app->getSecurity()->generatePasswordHash($password);
        
		  $user->generateAuthKey(); 
		
		 $user->save(false); // To exceute the query
		 
		 $user_id =  $user->auth_key;
		 
				$message = "<table border='1' cellpadding='10' cellspacing='10' width='100%' height='100'>
				<tr>
				<td><img class='logo_light' alt='Letstagon Logo' src='http://agathsya.com/letstagon/admin/Admin/assets/images/logo-icon.png'><img class='logo_light' alt='Letstagon Logo' src='http://agathsya.com/letstagon/admin/Admin/assets/images/logo-text.png'></td>
				</tr>
				<tr>
				<td width='100%' height='60'>
				<p></p>
				<p></p>";
				$message .= 'Greetings '.$user_name.',<br><br>';
				$message .= '<p>This is to inform that you have successfully registered in LetsTagOn.</p>';
				$message .= "<p>Please verify your email address so we know that it's really you!</p>";
				$message .= '<p><a style="font-size:16px;font-family:Helvetica,Helvetica neue,Arial,Verdana,sans-serif;font-weight:none;color:#ffffff;text-decoration:none;background-color:#3572b0;border-top:11px solid #3572b0;border-bottom:11px solid #3572b0;border-left:20px solid #3572b0;border-right:20px solid #3572b0;border-radius:5px;display:inline-block" class="m_1789912164564125250mobile-button" href="http://staging.letstagon.com/#/mail-activation/'.$user_id.'" target="_blank" data-saferedirecturl="">Verify my email address</a></p>';
				$message .= '<p><b>Happy volunteering,</b></p>';
				$message .= '<p><b>LetsTagOn team</b></p>';
				$message .="
				</td>
				</tr>
				<tr>
				<td width='100%' cellpadding='1' cellspacing='1' bgcolor='#DFDFDF' height='1'>
				</td>
				</tr>
				</table>";
		 Yii::$app->mailer->compose()
		->setFrom(['support@letstagon.com'=>'LetsTagOn'])
		->setTo($email)
		->setSubject('Registration Success')
		->setHtmlBody($message)
		->send(); 
		
		 // User::send_register_success_mail($email,$user_name); 
	   
		 /* Sending mail confitrmation using user model*/
		 return $result = ['status' => "success", 'message' => 'Registration successful. Activation link has been sent to your email. Please activate your account.'];
		 
	 }
	 else
	 {
		 return $result = ['status' => "fail", 'message' => 'User already exists'];
		 
	 } 
	 
}

	/* API For User Login */
	public function actionLogin()
	{
		 $model = new LoginForm();
		 $post = file_get_contents("php://input");
		 $request = Json::decode($post);
		 $email 	= $request['email'];
		 $password 	= $request['password'];
		 
		 if(empty($email && $password))
		 {
			 return $result = ['status' => "fail", 'message' => 'Please fill all the fields'];
			 
		 }
		$image_path = "http://staging.api.letstagon.com/profile_images/";
			  $identity =  User::find()->where(['email'=>$email])->one();
			 if(empty($identity))
			 {
				 return $result = ['status' => "fail", 'message' => 'Email not exists']; 
			 }
			 $user_activation = User::find()->where(['email'=>$email])->one();
				 if($user_activation->status == 0){
					return $result = ['status'=>'fail','message'=>"Activate the account in email"];
				 }
				  if($user_activation->role_type == 1){
					return $result = ['status'=>'fail','message'=>"Email not exists"];
				 }
			 $model->email = $identity->email;
			 $model->password = $password;
			 
			 if($model->validate())
			 {
				 $user_name = $identity->first_name .' '.$identity->last_name;
				 $out = ['user_id'=>$identity->id,'user_name'=>$user_name,'image_path'=>$image_path.$identity->image_path,'auth_key'=>$identity->auth_key];
				return $result = ['data' => $out,'status' => "success", 'message' => 'Login Success']; 
			 }
			 else
			 {
				  return $result = ['status' => "fail", 'message' => 'Invalid Password']; 
				 
			 }
	}

	public function actionUser_activation()
	{
		$post = file_get_contents("php://input");
	    $request 		= Json::decode($post);
		
	    $user_id 		= $request['user_id']; 
	 
		
			$identity =  User::find()->where(['auth_key'=>$user_id])->one();
			 if(empty($identity))
			 {
				 return $result = ['status' => "fail", 'message' => 'User not exists']; 
			 }
		
			$user_identity =  User::find()->where(['auth_key'=>$user_id,'status'=>'1'])->one();
			 if($user_identity)
			 {
				 return $result = ['status' => "fail", 'message' => 'User already activated']; 
			 }
			 
			$identity->status =1;
			$identity->save(false);
		
		return $result = ['status'=>'success','message'=>'Updated Successfully'];
	}
		public function actionForgetpassword(){

		$post = file_get_contents("php://input");
	    $request = Json::decode($post);
	    $email = $request['email'];
	    $identity = User::find()->select(['id','email','first_name','last_name','auth_key'])->where(['email'=>$email])->one();
	    if(empty($identity)){
	    	return $result = ['status'=>'fail','message'=>'email doesnot exists'];
	    }
		$user_name 	= $identity->first_name .' '.$identity->last_name;
	    $user_id = $identity->auth_key;
	    $message = "<table border='1' cellpadding='10' cellspacing='10' width='100%' height='100'>
				<tr>
				<td><img class='logo_light' alt='Letstagon Logo' src='http://agathsya.com/letstagon/admin/Admin/assets/images/logo-icon.png'><img class='logo_light' alt='Letstagon Logo' src='http://agathsya.com/letstagon/admin/Admin/assets/images/logo-text.png'></td>
				</tr>
				<tr>
				<td width='100%' height='60'>
				<p></p>
				<p></p>";
				$message .= 'Greetings '.$user_name.',<br><br>';
				$message .= '<p><a style="font-size:16px;font-family:Helvetica,Helvetica neue,Arial,Verdana,sans-serif;font-weight:none;color:#ffffff;text-decoration:none;background-color:#3572b0;border-top:11px solid #3572b0;border-bottom:11px solid #3572b0;border-left:20px solid #3572b0;border-right:20px solid #3572b0;border-radius:5px;display:inline-block" class="m_1789912164564125250mobile-button" href="http://staging.letstagon.com/#/reset-password/'.$user_id.'" target="_blank" data-saferedirecturl="">Reset Password</a></p>';
				$message .= '<p><b>Happy volunteering,</b></p>';
				$message .= '<p><b>LetsTagOn team</b></p>';
				$message .="
				</td>
				</tr>
				<tr>
				<td width='100%' cellpadding='1' cellspacing='1' bgcolor='#DFDFDF' height='1'>
				</td>
				</tr>
					</table>";
			 Yii::$app->mailer->compose()
			->setFrom(['support@letstagon.com'=>'LetsTagOn'])
			->setTo($email)
			->setSubject('Reset Password')
			->setHtmlBody($message)
			->send(); 
			$identity->reset_flag = 1;
			$identity->save(false);
		 return $result = ['status' => "success", 'message' => 'Reset password link has been sent to your email'];
	}
	
	public function actionAdminforgetpassword()
	{

		$post = file_get_contents("php://input");
	    $request = Json::decode($post);
	    $email = $request['email'];
	    $identity = User::find()->select(['id','email','first_name','last_name','auth_key'])->where(['email'=>$email])->one();
	    if(empty($identity)){
	    	return $result = ['status'=>'fail','message'=>'email doesnot exists'];
	    }
		$user_name 	= $identity->first_name .' '.$identity->last_name;
	    $user_id = $identity->auth_key;
	    $message = "<table border='1' cellpadding='10' cellspacing='10' width='100%' height='100'>
				<tr>
				<td><img class='logo_light' alt='Letstagon Logo' src='http://agathsya.com/letstagon/admin/Admin/assets/images/logo-icon.png'><img class='logo_light' alt='Letstagon Logo' src='http://agathsya.com/letstagon/admin/Admin/assets/images/logo-text.png'></td>
				</tr>
				<tr>
				<td width='100%' height='60'>
				<p></p>
				<p></p>";
				$message .= 'Greetings '.$user_name.',<br><br>';
				$message .= '<p><a style="font-size:16px;font-family:Helvetica,Helvetica neue,Arial,Verdana,sans-serif;font-weight:none;color:#ffffff;text-decoration:none;background-color:#3572b0;border-top:11px solid #3572b0;border-bottom:11px solid #3572b0;border-left:20px solid #3572b0;border-right:20px solid #3572b0;border-radius:5px;display:inline-block" class="m_1789912164564125250mobile-button" href="http://staging.letstagon.com/admin/#/reset-password/'.$user_id.'" target="_blank" data-saferedirecturl="">Reset Password</a></p>';
				$message .= '<p><b>Happy volunteering,</b></p>';
				$message .= '<p><b>LetsTagOn team</b></p>';
				$message .="
				</td>
				</tr>
				<tr>
				<td width='100%' cellpadding='1' cellspacing='1' bgcolor='#DFDFDF' height='1'>
				</td>
				</tr>
					</table>";
			 Yii::$app->mailer->compose()
			->setFrom(['support@letstagon.com'=>'LetsTagOn'])
			->setTo($email)
			->setSubject('Reset Password')
			->setHtmlBody($message)
			->send(); 
			$identity->reset_flag = 1;
			$identity->save(false);
		 return $result = ['status' => "success", 'message' => 'Reset password link has been sent to your email'];
	}
	
	
		public function actionResetpassword(){

		$post = file_get_contents("php://input");
	    $request = Json::decode($post);
	    $id = $request['id'];
	    $password = $request['password'];
	    $identity = User::find()->select(['id'])->where(['auth_key'=>$id])->one();
	    if(empty($identity)){
	    	return $result = ['status'=>'fail','message'=>'User id does not exists'];
	    }
	    $identity->password_hash = Yii::$app->getSecurity()->generatePasswordHash($password);
	    $identity->reset_flag = 0;
	    $identity->save(false);
	    return $result = ['status'=>'success','message'=>'Password reset successful'];
	}
	
	public function actionCheckresetstatus(){

		$post = file_get_contents("php://input");
	    $request = Json::decode($post);
	    $id = $request['id'];

	    $checkstatus = User::find()->select(['reset_flag'])->where(['auth_key'=>$id])->scalar();
	    if($checkstatus == 0){
	    	return $result = ['status'=>'fail','message'=>'Link has expired'];
	    }
	    return $result = ['status'=>'success','message'=>'Password reset successful'];
	}
	
	/* API For admin Login */
	
	public function actionAdminlogin()
	{
		 $model = new LoginForm();
		 $post = file_get_contents("php://input");
		 $request = Json::decode($post);
		 $email 	= $request['email'];
		 $password 	= $request['password'];
		 
		 if(empty($email && $password))
		 {
			 return $result = ['status' => "fail", 'message' => 'Please fill all the fields'];
			 
		 }
		
			  $identity =  User::find()->where(['email'=>$email])->one();
			 if(empty($identity))
			 {
				 return $result = ['status' => "fail", 'message' => 'Email not exists']; 
			 }
			 $user_activation = User::find()->select(['status','role_type'])->where(['email'=>$email])->one();
			
			 if($user_activation->role_type == "2"){
				return $result = ['status'=>'fail','message'=>'Login failed'];
			 }
			 $model->email = $identity->email;
			 $model->password = $password;
			 
			 if($model->validate())
			 {
				 $user_name = $identity->first_name .' '.$identity->last_name;
				 $out = ['user_id'=>$identity->id,'user_name'=>$user_name];
				return $result = ['data' => $out,'status' => "success", 'message' => 'Login Success']; 
			 }
			 else
			 {
				  return $result = ['status' => "fail", 'message' => 'Invalid Password']; 
				 
			 }
	}
	
	/* API for User Profile */
	
	public function actionUser_profile()
	{
		$post = file_get_contents("php://input");
	    $request 		= Json::decode($post);
		
	    $user_id 		= $request['user_id']; 
		  $imagepath = "http://staging.api.letstagon.com/profile_images/";
			$identity =  User::find()->where(['id'=>$user_id])->one();
			 if(empty($identity))
			 {
				 return $result = ['status' => "fail", 'message' => 'User not exists']; 
			 }
		
			 $user_name = $identity->first_name .' '.$identity->last_name;
			  $created_on = date('d-m-Y H:i', strtotime($identity->created_at));
			  $dob = date('d-m-Y', strtotime($identity->dob));
			 
				 $out = [
							'user_id'		=>	$identity->id,
							'first_name'	=>	$identity->first_name,
							'last_name'		=>	$identity->last_name,
							'phone'			=>	$identity->phone,
							'email'			=>	$identity->email,
							'dob'			=>	$dob,
							'designation'	=>	$identity->designation,
							'organization'	=>	$identity->organization,
							'file' 			=>  $imagepath.$identity->image_path,
							'status'		=>  $identity->status,
							'username'		=>$identity->first_name.' '.$identity->last_name
							
						];
			return $result = ['data' => $out,'status' => "success", 'message' => 'Individual User Profile']; 
	}
	
	public function actionUser_profile1()
	{
		$post = file_get_contents("php://input");
	    $request 		= Json::decode($post);
		
	    $user_id 		= $request['user_id']; 
		  $imagepath = "http://staging.api.letstagon.com/profile_images/";
			$identity =  User::find()->where(['id'=>$user_id])->one();
			 if(empty($identity))
			 {
				 return $result = ['status' => "fail", 'message' => 'User not exists']; 
			 }
		
			 $user_name = $identity->first_name .' '.$identity->last_name;
			  $created_on = date('d-m-Y H:i', strtotime($identity->created_at));
			  $dob = date('Y-m-d', strtotime($identity->dob));
			 
				 $out = [
							'user_id'		=>	$identity->id,
							'first_name'	=>	$identity->first_name,
							'last_name'		=>	$identity->last_name,
							'phone'			=>	$identity->phone,
							'email'			=>	$identity->email,
							'dob'			=>	$dob,
							'designation'	=>	$identity->designation,
							'organization'	=>	$identity->organization,
							'file' 			=>  $imagepath.$identity->image_path,
							'skills'		=>	$identity->skills,
							'interests'		=>	$identity->interest,
							'username'		=> $identity->first_name.''.$identity->last_name
							
						];
			return $result = ['data' => $out,'status' => "success", 'message' => 'Individual User Profile']; 
	}
	
		/* To edit user profile */
	
	public function actionEdit_user_profile()
	{
		$post = file_get_contents("php://input");
	    $request 		= Json::decode($post);
		 $first_name 	= $request['first_name'];
		 $last_name 	= $request['last_name'];
		 $phone 		= $request['phone'];
		 $dob 			= $request['dob'];
		 $designation 	= $request['designation'];
		 $organization 	= $request['organization']; 
		 $user_id 		= $request['user_id'];
		 
		
	    $model = User::find()->where(['id'=>$user_id ])->one(); 
		if(empty($model))
		{
			return $result = ['status'=>'fail','message'=>'No User found'];
		}
		$images = UploadedFile::getInstancesByName("file");
        if(!empty($images))
        {
		    foreach ($images as $image) {

		        $filename = $image->name;
		        $path = "../../profile_images/" . $filename;
		        $image->saveAs($path);//saving image
		    }
        }
        else{
        		$filename = $model->image_path;
        }
		$image_path = "http://staging.api.letstagon.com/profile_images/";
		$user_name 	= $first_name .' '.$last_name;
		$model->first_name		=	$first_name;
		$model->last_name		=	$last_name;
		$model->phone			=	$phone;
		$model->dob			=	$dob;
		$model->designation	=	$designation;
		$model->organization	=	$organization;
		$model->image_path     =    $filename;
	    $model->save(false);
		$out=['userid'=>$user_id,'user_name'=>$user_name,'image_path'=>$image_path.$filename];
		return $result = ['data'=>$out,'status'=>'success','message'=>'User Profile Updated Successfully'];
	}
	
		/* API for change of password */
	
	public function actionChange_user_password()
	{
		$model = new LoginForm();
		$post 		= file_get_contents("php://input");
	    $request 	= Json::decode($post);
		
		 $current_password 	= $request['current_password'];
		 $new_password 		= $request['new_password'];
		 $confirm_password 	= $request['confirm_password'];
		 
		 $user_id 			= $request['user_id'];
		 
		 if($new_password != $confirm_password) 
		 {
			 return $result = ['status'=>'fail','message'=>'New password and confirm password not matching'];
		 }
		 
		   $identity =  User::find()->where(['id'=>$user_id])->one();
			 if(empty($identity))
			 {
				 return $result = ['status' => "fail", 'message' => 'User not exists']; 
			 }
			 
			 $model->email = $identity->email;
			 $model->password = $current_password;
			 
			 if($model->validate())
			 { 
					 
			$identity->password_hash = Yii::$app->getSecurity()->generatePasswordHash($new_password); 
			$identity->save(false); // To exceute the query
				
				return $result = ['status' => "success", 'message' => 'Password changed successfully']; 
			 }
			 else
			 {
				  return $result = ['status' => "fail", 'message' => 'Current password not matching with our record']; 
				 
			 } 
		
	}
	
	/*Api to Join user applied with event and master View data */
	 
	public function actionIndividual_opportunity_list()
	{ 
		$post = file_get_contents("php://input");
	    $request 		= Json::decode($post);
		$userid = $request['user_id'];
		$opp_list = "SELECT ut.id as user_tag_id,ut.created_at,ut.attendance_flag,u.first_name,u.last_name,u.email,u.phone,
		e.id as event_id,e.experience_id,e.date_scheduled,e.mode,
		e.event_duration,me.experience_name,me.cause,me.id,
        	e.address,s.state_name,c.city_name
						FROM user_tagged_experiences as ut
                          	JOIN user as u ON 
							ut.user_id = u.id
                                JOIN events as e ON 
                                ut.event_id = e.id
                                	JOIN master_experience as me ON 
									e.experience_id = me.id
                                        JOIN states as s ON 
										e.state_id = s.id 
											JOIN cities as c ON 
											e.city_id = c.id
											where ut.user_id = '".$userid."' and (ut.attendance_flag='0' OR ut.attendance_flag='1')
                                            order by ut.created_at desc";
		$command = Yii::$app->db->createCommand($opp_list);
		$opp_list_result = $command->queryAll();
				
		// print_r($events);exit;
		 
		if(empty($opp_list_result))
		{
			return $result = ['status'=>'fail','message'=>'No data'];
		}
		
		$array = [];
		
		$i=1;
		foreach($opp_list_result as $opp_res)
		{
			$out = [
				'index'				=>	$i,
				'user_tag_id'		=>	$opp_res['user_tag_id'],
				'user_name'			=>	$opp_res['first_name'].' '.$opp_res['last_name'],
				'phone'				=>	$opp_res['phone'],
				'experience_name'	=>	$opp_res['experience_name'],
				'cause_name'		=>	$opp_res['cause'], 
				'mode'				=>	$opp_res['mode'], 
				'event_date'		=>	$opp_res['date_scheduled'],
				'event_duration'	=>	$opp_res['event_duration'], 
				'address'			=>	$opp_res['address'],
				'state_name'		=>	$opp_res['state_name'],
				'city_name'			=>	$opp_res['city_name'],	
				'attendance_flag'	=>	$opp_res['attendance_flag'],
				'event_id'			=>   $opp_res['event_id'],
				'experience_id'		=>	$opp_res['id']
				
			];
			
			array_push($array,$out);
			$i++;
			
		}
		
		return $result = ['content'=>$array,'status'=>'success','message'=>'list of volunteers tagged'];
	}


	/* Individual Attended Opp */
	
	public function actionIndividual_attended_opportunity_list()
	{ 
		 $post 		= file_get_contents("php://input");
	     $request 	= Json::decode($post);
		 
		 $userid 	= $request['user_id'];
	
		$opp_list = "SELECT ut.id as user_tag_id,ut.created_at,ut.attendance_flag,u.first_name,u.last_name,u.email,u.phone,
		e.id as event_id,e.experience_id,e.date_scheduled,e.mode,e.experience_id,
		e.event_duration,me.experience_name,me.cause,
        	e.address,s.state_name,c.city_name
						FROM user_tagged_experiences as ut
                          	JOIN user as u ON 
							ut.user_id = u.id
                                JOIN events as e ON 
                                ut.event_id = e.id
                                	JOIN master_experience as me ON 
									e.experience_id = me.id
                                        JOIN states as s ON 
										e.state_id = s.id 
											JOIN cities as c ON 
											e.city_id = c.id
											where ut.user_id = '".$userid."' and ut.attendance_flag='2'
                                            order by ut.created_at desc";
		$command = Yii::$app->db->createCommand($opp_list);
		$opp_list_result = $command->queryAll();
				
		// print_r($events);exit;
		 
		if(empty($opp_list_result))
		{
			return $result = ['status'=>'fail','message'=>'No applied oppurtunities'];
		}
		
		$array = [];
		
		$i = 1;
		
		foreach($opp_list_result as $opp_res)
		{
			$out = [
				'index'				=>	$i,
				'user_tag_id'		=>	$opp_res['user_tag_id'],
				'user_name'			=>	$opp_res['first_name'].' '.$opp_res['last_name'],
				'phone'				=>	$opp_res['phone'],
				'experience_name'	=>	$opp_res['experience_name'],
				'cause_name'		=>	$opp_res['cause'], 
				'mode'				=>	$opp_res['mode'], 
				'event_date'		=>	date('d M Y H:m', strtotime($opp_res['date_scheduled'])),
				'cause'				=>	$opp_res['cause'], 
				'event_duration'	=>	$opp_res['event_duration'], 
				'address'			=>	$opp_res['address'],
				'state_name'		=>	$opp_res['state_name'],
				'city_name'			=>	$opp_res['city_name'],	
				'attendance_flag'	=>	$opp_res['attendance_flag'],	
				'event_id'			=>  $opp_res['event_id'],
				'experience_id'     =>  $opp_res['experience_id']
			];
			
			array_push($array,$out);
			$i++;
		}
		
		return $result = ['content'=>$array,'status'=>'success','message'=>'list of applied opportunities'];
	}
	
	public function actionUser_event_details()
	{ 
		$post = file_get_contents("php://input");
		$request = Json::decode($post);
		
		$event_id	= $request['event_id'];
		 
		$image_path = "http://staging.api.letstagon.com/oppurtunity_images/";
		
		$event_info = Events::find()->where(['id'=>$event_id])->one();
	 
		if(empty($event_info))
		{
			return $result = ['status'=>'fail','message'=>'No such Event'];
		}
		 
		$eventlist = "SELECT e.id,me.id as experience_id,e.event_type,e.schedule_type,e.participation_type,e.mode,
						e.no_of_volunteers,e.date_scheduled,
						e.event_duration,e.address,me.experience_name,me.cause,
						me.description,me.image_path,s.state_name,c.city_name,e.state_id,e.city_id
						FROM events as e
                          JOIN master_experience as me ON 
							e.experience_id = me.id  
								JOIN states as s ON 
								e.state_id = s.id 
									JOIN cities as c ON 
									e.city_id = c.id  where e.id = $event_id";
		$command = Yii::$app->db->createCommand($eventlist);
		$events = $command->queryAll();
				
		 $event = $events[0];
		   
		 
			$out = [
				'id'				=>	$event['id'],
				'experience_id'		=>	$event['experience_id'],
				'experience_name'	=>	$event['experience_name'],
				'event_type'		=>	$event['event_type'],
				'schedule_type'		=>	$event['schedule_type'],
				'participation_type'=>	$event['participation_type'],
				'mode'				=>	$event['mode'],
				'cause'				=>	$event['cause'],
				'no_of_volunteers'	=>	$event['no_of_volunteers'],
				'date_scheduled'	=>	date('d F Y H:i', strtotime($event['date_scheduled'])), 
				'event_duration'	=>	$event['event_duration'], 
				'address'			=>	$event['address'],
				'state_name'		=>	$event['state_name'],
				'city_name'			=>	$event['city_name'],
				'description'		=>	$event['description'],
				'image_path'		=>	$image_path.''.$event['image_path']
					
			];
			   
		return $result = ['content'=>$out,'status'=>'success','message'=>'event detials'];
	}
	
	public function actionInsert_requested_opportunity()
	{ 
		 $post = file_get_contents("php://input");
		 $request = Json::decode($post);
		  
		 
		 $user_id 			= $request['user_id'];
		 $event_id 			= $request['event_id'];
		 
		 $organisation 		= $request['organisation'];
		 $request_date 		= $request['request_date'];
		 $no_of_volunteers 	= $request['no_of_volunteers'];
		 $description 		= $request['description'];
		 $address 			= $request['location'];
		 $experience_identity =  Events::find()->where(['id'=>$event_id])->one();
		 if(empty($experience_identity))
		 {
			 return $result = ['status' => "fail", 'message' => 'No such events exists']; 
		 }
		 
			$identity =  User::find()->where(['id'=>$user_id])->one();
			 if(empty($identity))
			 {
				 return $result = ['status' => "fail", 'message' => 'User not exists']; 
			 }
			 
		$requested_opp = LtgRequestedOpportunities::find()->where(['user_id'=>$user_id,'event_id'=>$event_id])->one();
		 if(!empty($requested_opp))
		 {
		 	return $result = ['status' => "fail", 'message' => 'Already requested for the opportunity'];
		 } 
		 
			$model 			= 	new LtgRequestedOpportunities();  // Storing new model record in a variable
		 
			 $model->event_id			=	$event_id;
			 $model->user_id			=	$user_id;
			 $model->organisation		=	$organisation;
			 $model->request_date		=	$request_date;
			 $model->no_of_volunteers	=	$no_of_volunteers;
			 $model->description		=	$description;
			 $model->address			=	$address;
			 $model->save(false); // To exceute the query
			 
			 return $result = ['status' => "success", 'message' => 'Thank you for your request. We will get back to you soon.'];
		  
	}
	//API for testimonials
	public function actionAdd_testimonials(){

		$post = file_get_contents("php://input");
	    $request = Json::decode($post);
	    $user_id = $request['user_id'];
	    $message = $request['message'];
	    $subject = $request['subject'];
	    $user = User::find()->where(['id'=>$user_id])->scalar();
	    if(empty($user)){
	    	
	    	return $result = ['status'=>'fail','message'=>'User not found'];
	    }
	    $model = new Testimonials();
	    $model->user_id = $user_id;
	    $model->subject = $subject;
	    $model->message = $message;
	    $model->save(false);
	    return $result = ['status'=>'success','message'=>'Testimonials added'];
	}

	//View testimonials
	public function actionView_testimonials(){

		$post = file_get_contents("php://input");
	    $request = Json::decode($post);
	    $testimonialslist = "SELECT t.id,t.message,t.subject,u.image_path,u.first_name,u.last_name,u.designation FROM testimonials as t JOIN user as u ON t.user_id = u.id";
		$testimonials = Yii::$app->db->createCommand($testimonialslist);
		$testimonialslists = $testimonials->queryAll();
		$image_path = "http://staging.api.letstagon.com/profile_images/";
		if(empty($testimonialslists)){
			return $result = ['status'=>'fail','message'=>'No testimonials found'];
		}
		$array=[];
		foreach($testimonialslists as $testimonialslist){
			$out=[
			'first_name'=>$testimonialslist['first_name'],
			'last_name'=>$testimonialslist['last_name'],
			'message' => $testimonialslist['message'],
			'subject'  => $testimonialslist['subject'],
			'id'       => $testimonialslist['id'],
			'designation'=>$testimonialslist['designation'],
			'image_path' => $image_path.$testimonialslist['image_path']
		];
		array_push($array,$out);
		}
		
			return $result = ['data'=>$array,'status'=>'success','message'=>'List of Testimonails'];
		}
	//View Individual Testimonials
	public function actionView_individual_testimonial(){

		$post = file_get_contents("php://input");
	    $request = Json::decode($post);
	    $user_id = $request['user_id'];
		$image_path = "http://staging.api.letstagon.com/profile_images/";
	    $user = User::find()->where(['id'=>$user_id])->one();
	    if(empty($user)){

	    	return $result= ['status'=>'fail','message'=>'User not found'];
	    }
	    $testimonialslist = "SELECT t.id,t.message,t.subject,u.image_path,u.first_name,u.last_name,u.designation FROM testimonials as t JOIN user as u ON t.user_id = u.id where t.user_id = '".$user_id."'";
		$testimonials = Yii::$app->db->createCommand($testimonialslist);
		$testimonialslist = $testimonials->queryAll();

		if(empty($testimonialslist)){
			return $result = ['status'=>'fail','message'=>'No testimonials found'];
		}
		$testimonialslists = $testimonialslist[0];
		$out=[
			'first_name'=>$testimonialslists['first_name'],
			'last_name'=>$testimonialslists['last_name'],
			'message' => $testimonialslists['message'],
			'subject'  => $testimonialslists['subject'],
			'id'       => $testimonialslists['id'],
			'designation'=>$testimonialslists['designation'],
			'image_path' => $image_path.$testimonialslists['image_path']
		];
		return $result = ['data'=>$out,'status'=>'success','message'=>'List of Testimonails'];
	}

	//Deleting Testimonial based on id
	public function actionDelete_testimonial()
	{

		$post = file_get_contents("php://input");
	    $request = Json::decode($post);
	    $id = $request['id'];

	    $testimonail = Testimonials::find()->where(['id'=>$id])->one();

	    if(empty($testimonail)){

	    	return $result = ['status'=>'fail','message'=>'Testimonial not found'];
	    }

	    $testimonail->delete();

	    return $result = ['status'=>'success','message'=>'Testimonial Deleted'];
	}

	//Edit Testimonial
	public function actionEdit_testimonial()
	{

		$post = file_get_contents("php://input");
	    $request = Json::decode($post);
	    $id = $request['id'];
	    $user_id = $request['user_id'];
	    $subject = $request['subject'];
	    $message = $request['message'];
	    $testimonail = Testimonials::find()->where(['id'=>$id,'user_id'=>$user_id])->one();

	    if(empty($testimonail)){

	    	return $result = ['status'=>'fail','message'=>'Testimonial not found'];
	    }
	    $testimonail->subject = $subject;
	    $testimonail->message = $message;
	    $testimonail->save(false);
	    return $result = ['status'=>'success','message'=>'Testimonial edit successful'];
	}
	//Editing specific testimonial
	public function actionView_editing_testimonial()
	{
		$post = file_get_contents("php://input");
		$request = Json::decode($post);
		$user_id = $request['user_id'];
		$user = User::find()->where(['id'=>$user_id])->one();
		if(empty($user)){

			return $result= ['status'=>'fail','message'=>'User not found'];
		}
		$testimonialslist = "SELECT t.id,t.message,t.subject,u.image_path,u.first_name,u.last_name,u.designation,t.user_id FROM testimonials as t JOIN user as u ON t.user_id = u.id where t.user_id = '".$user_id."'";
		$testimonials = Yii::$app->db->createCommand($testimonialslist);
		$testimonialslist = $testimonials->queryAll();

		if(empty($testimonialslist)){
			return $result = ['status'=>'fail','message'=>'No testimonials found'];
		}
		$testimonialslists = $testimonialslist[0];
		$out=[
			'message' => $testimonialslists['message'],
			'subject'  => $testimonialslists['subject'],
			'id'       => $testimonialslists['id'],
			'user_id'  =>$testimonialslists['user_id']
		];
		return $result = ['data'=>$out,'status'=>'success','message'=>'List of Testimonails'];
	}
	
	//Api for event reviews
	public function actionReviews(){
		$post = file_get_contents("php://input");
		$request = Json::decode($post);
		$user_id = $request['user_id'];
		$message = $request['message'];
		$rating = $request['rating'];
		$subject = $request['subject'];
		$experience_id = $request['experience_id'];
		$eventid = $request['eventid'];
		$user = User::find()->where(['id'=>$user_id])->one();
		if(empty($user)){

			return $result= ['status'=>'fail','message'=>'User not found'];
		}
		$event = Events::find()->where(['id'=>$eventid])->one();
		if(empty($event)){
			return $result= ['status'=>'fail','message'=>'Event not found'];
		}
		
		$model = new Reviews();
		
		$model->user_id = $user_id;
		$model->message = $message;
		$model->rating = $rating;
		$model->subject = $subject;
		$model->experience_id = $experience_id;
		$model->event_id = $eventid;
		$model->save(false);
		return $result = ['status'=>'success','message'=>'Review Added Successfully'];	
	}
	
	//Review List
	
	public function actionReviewlist(){
		$post = file_get_contents("php://input");
		$request = Json::decode($post);
		$event_id = $request['event_id'];
		$reviewslist = "SELECT r.id,r.message,r.subject,r.rating,u.first_name,u.last_name,u.designation,e.date_scheduled,e.address,me.experience_name FROM reviews as r 
		JOIN events as e ON e.id = r.event_id 
		JOIN master_experience as me ON me.id = r.experience_id
		JOIN user as u ON u.id = r.user_id
		where r.event_id = '".$event_id."'";
		$review = Yii::$app->db->createCommand($reviewslist);
		$reviews = $review->queryAll();
		
		if(empty($reviews)){
			
			return $result = ['status'=>'fail','message'=>'No reviews found for this event'];
		}
		
		return $result = ['data'=>$reviews,'status'=>'success','messages'=>'List of reviews'];
	}
	
	public function actionReviewuser(){
		$post = file_get_contents("php://input");
		$request = Json::decode($post);
		$event_id = $request['event_id'];
		$user_id = $request['user_id'];
		$reviewslist = "SELECT r.id,r.message,r.subject,r.rating,u.first_name,u.last_name,u.designation,e.date_scheduled,e.address,me.experience_name,me.image_path FROM reviews as r 
		JOIN events as e ON e.id = r.event_id 
		JOIN master_experience as me ON me.id = r.experience_id
		JOIN user as u ON u.id = r.user_id
		where r.event_id = '".$event_id."' AND r.user_id = '".$user_id."'";
		$review = Yii::$app->db->createCommand($reviewslist);
		$reviews = $review->queryAll();
		
		$image_path = "http://staging.api.letstagon.com/oppurtunity_images/";
		if(empty($reviews)){
			
			return $result = ['status'=>'fail','message'=>'No reviews found for this event'];
		}
		$reviews = $reviews[0];
		$out = [
				"id"=>$reviews['id'],
				"message"=>$reviews['message'],
				"subject"=>$reviews['subject'],
				"rating"=>$reviews['rating'],
				"first_name"=>$reviews['first_name'],
				"last_name"=>$reviews['last_name'],
				"designation"=>$reviews['designation'],
				"date_scheduled"=>$reviews['date_scheduled'],
				"address"=>$reviews['address'],
				"experience_name"=>$reviews['experience_name'],
				"image_path"=>$image_path.$reviews['image_path']
			
		];
		
		return $result = ['data'=>$out,'status'=>'success','messages'=>'List of reviews'];
	}
	public function actionUpdate_review(){
		$post = file_get_contents("php://input");
		$request = Json::decode($post);
		$user_id = $request['user_id'];
		$message = $request['message'];
		$rating = $request['rating'];
		$subject = $request['subject'];
		$experience_id = $request['experience_id'];
		$event_id = $request['event_id'];
		$user = User::find()->where(['id'=>$user_id])->one();
		if(empty($user)){

			return $result= ['status'=>'fail','message'=>'User not found'];
		}
		$reviews = Reviews::find()->where(['user_id'=>$user_id,'event_id'=>$event_id,'experience_id'=>$experience_id])->one();
		if(empty($reviews)){
			return $result= ['status'=>'fail','message'=>'Event not found'];
		}
		$reviews->message = $message;
		$reviews->rating = $rating;
		$reviews->subject = $subject;
		$reviews->save(false);
		return $result = ['status'=>'success','message'=>'Review Updated Successfully'];	
	}
	
	public function actionEdit_review_information(){
		$post = file_get_contents("php://input");
		$request = Json::decode($post);
		$user_id = $request['user_id'];
		$experience_id = $request['experience_id'];
		$event_id = $request['event_id'];
		$user = User::find()->where(['id'=>$user_id])->one();
		if(empty($user)){

			return $result= ['status'=>'fail','message'=>'User not found'];
		}
		$reviews = Reviews::find()->select(['subject','message','rating'])->where(['user_id'=>$user_id,'event_id'=>$event_id,'experience_id'=>$experience_id])->one();
		if(empty($reviews)){
			return $result= ['status'=>'fail','message'=>'Event not found'];
		}
		return $result = ['data'=>$reviews,'status'=>'success','message'=>'Review Updated Successfully'];	
	}
	
		public function actionGetexperiencedetails(){
		$post = file_get_contents("php://input");
		$request = Json::decode($post);
		$event_id = $request['event_id'];
		$experience = "SELECT e.id,e.date_scheduled,e.address,me.experience_name FROM events as e 
		JOIN master_experience as me ON me.id = e.experience_id
		where e.id = '".$event_id."'";
		$experiences = Yii::$app->db->createCommand($experience);
		$experiencelist = $experiences->queryAll();
		
		if(empty($experiencelist)){
			
			return $result = ['status'=>'fail','message'=>'No event'];
		}
		
		return $result = ['data'=>$experiencelist,'status'=>'success','messages'=>'Event Information'];
	}
	
	public function actionSocial_media_register()
  {
	 $post = file_get_contents("php://input");
     $request = Json::decode($post);
	 
	 $first_name 	= $request['first_name'];
	 $last_name 	= "";
	 $email 		= $request['email'];
	 $app_id 		= $request['app_id'];
	 $password 		= "Socialmedia";
	 $phone 		= "";
	 $dob 			= "";
	 $designation 	= "";
	 $social_media 	= "1";
	 $organization 	= ""; 
	 $role_type 	= '2';
	 $status 		= '0';
	 $user_name 	= $first_name .' '.$last_name;
	 $imagepath = "http://staging.api.letstagon.com/profile_images/";
	 $identity =  User::find()->select(['id','first_name','last_name','email','app_id','image_path','social_media_flag'])->where(['app_id'=> $app_id ])->one();
	 
	 if(empty($identity)){
		 
		$user = new User();  // Storing new model record in a variable
		$user->first_name		=	$first_name;
		$user->last_name		=	$last_name;
		$user->email			=	$email;
		$user->phone			=	$phone;
		$user->dob				=	$dob;
		$user->designation		=	$designation;
		$user->organization		=	$organization;
		$user->role_type		=	$role_type;
		$user->app_id			=	$app_id;
		$user->status			=	$status;
		$user->social_media_flag = 	$social_media;
		$user->image_path = 	     "default_icon.jpg";
		$user->password_hash = Yii::$app->getSecurity()->generatePasswordHash($password);
        
		  $user->generateAuthKey(); 
		
		 $user->save(false); // To exceute the query
		 
		 $user_id =  $user->id;
		 
				$message = "<table border='1' cellpadding='10' cellspacing='10' width='100%' height='100'>
				<tr>
				<td><img class='logo_light' alt='Letstagon Logo' src='http://agathsya.com/letstagon/admin/Admin/assets/images/logo-icon.png'><img class='logo_light' alt='Letstagon Logo' src='http://agathsya.com/letstagon/admin/Admin/assets/images/logo-text.png'></td>
				</tr>
				<tr>
				<td width='100%' height='60'>
				<p></p>
				<p></p>";
				$message .= 'Greetings '.$user_name.',<br><br>';
				$message .= '<p>This is to inform that you have successfully registered in LetsTagOn.</p>';
				$message .= "<p>Please verify your email address so we know that it's really you!</p>";
				$message .= '<p><b>Happy volunteering,</b></p>';
				$message .= '<p><b>LetsTagOn team</b></p>';
				$message .="
				</td>
				</tr>
				<tr>
				<td width='100%' cellpadding='1' cellspacing='1' bgcolor='#DFDFDF' height='1'>
				<p align='center'>	Website: <a href='http://letstagon.com/'>LetsTagOn</a></font></td>
				</tr>
				</table>";
		 Yii::$app->mailer->compose()
		->setFrom(['support@letstagon.com'=>'LetsTagOn'])
		->setTo($email)
		->setSubject('Registration Success')
		->setHtmlBody($message)
		->send(); 
		
		 // User::send_register_success_mail($email,$user_name); 
	   
		 /* Sending mail confitrmation using user model*/
		
		 $out= [
		 'user_id'=>$user->id,
		 'user_name'=>$user->first_name,
		 'image_path'=>$imagepath.$user->image_path,
		 'social_media_flag'=>$user->social_media_flag
		 ];
		 return $result = ['data'=>$out,'status' => "success", 'message' => 'Registration Succesfull'];
	 }

	 else
	 {
		
		 $out= [
		 'user_id'=>$identity->id,
		 'user_name'=>$identity->first_name,
		 'image_path'=>$imagepath.$identity->image_path	
		 ];
		 return $result = ['data'=>$out,'status' => "success", 'message' => 'Already registered'];
		 
	 } 
	 
}

	public function actionDashboard_count(){
	
	$post = file_get_contents("php://input");
     $request = Json::decode($post);
	$user_id = $request['user_id'];
	$array = [];
	$taggedexperiences = UserTaggedExperiences::find()->where(['user_id'=>$user_id,'attendance_flag'=>0])->count();
	$attendedexperiences = UserTaggedExperiences::find()->where(['user_id'=>$user_id,'attendance_flag'=>2])->count();
	$eventhours = UserTaggedExperiences::find()->select(['event_id'])->where(['user_id'=>$user_id])->all();
	foreach($eventhours as $event){
		$eventhours = Events::find()->select(['event_duration'])->where(['id'=>$event->event_id])->scalar();
		array_push($array,$eventhours);
	}
	
	$out = [
		'applied_opportunities'=>$taggedexperiences,
		'attended_opportunities'=>$attendedexperiences,
		'total_opportunities'=>$taggedexperiences + $attendedexperiences,
		'event_hours'=>array_sum($array)
	];
	return $result = ['data'=>$out,'status'=>'success','message'=>'Dashboard Count'];
		
	}
		
	public function actionRequested_user_opportunity_list()
	{
		 $post = file_get_contents("php://input");
		 $request = Json::decode($post);
		  $user_id 			= $request['user_id'];
		 
		 $identity =  User::find()->where(['id'=>$user_id])->one();
			 if(empty($identity))
			 {
				 return $result = ['status' => "fail", 'message' => 'User not exists']; 
			 }
		 
		$opp_list = "SELECT rq.id as request_id,rq.created_on,rq.status_flag,rq.organisation,rq.request_date,rq.no_of_volunteers,rq.description,u.first_name,u.last_name,u.email,u.phone,
		e.id as event_id,e.experience_id,e.date_scheduled,e.schedule_type,
		e.event_duration,me.experience_name,me.cause,
        	rq.address,s.state_name,c.city_name
						FROM ltg_requested_opportunities as rq
                          	JOIN user as u ON 
							rq.user_id = u.id
                                JOIN events as e ON 
                                rq.event_id = e.id
                                	JOIN master_experience as me ON 
									e.experience_id = me.id
                                        JOIN states as s ON 
										e.state_id = s.id 
											JOIN cities as c ON 
											e.city_id = c.id
											where rq.user_id = $user_id
                                            order by rq.created_on desc";
		$command = Yii::$app->db->createCommand($opp_list);
		$opp_list_result = $command->queryAll();
				
		// print_r($events);exit;
		 
		if(empty($opp_list_result))
		{
			return $result = ['status'=>'fail','message'=>'No data'];
		}
		
		$array = [];
		
		$i=1;
		foreach($opp_list_result as $opp_res)
		{
			$out = [
				'index'				=>	$i,
				'user_tag_id'		=>	$opp_res['request_id'],
				'user_name'			=>	$opp_res['first_name'].' '.$opp_res['last_name'],
				'email'				=>	$opp_res['email'],
				'phone'				=>	$opp_res['phone'],
				'experience_name'	=>	$opp_res['experience_name'], 
				'schedule_type'		=>	$opp_res['schedule_type'], 
				'organisation'		=>	$opp_res['organisation'], 
				'request_date'		=>	$opp_res['request_date'], 
				'req_no_of_volunteers'	=>$opp_res['no_of_volunteers'], 
				'description'		=>	$opp_res['description'], 
				'address'			=>	$opp_res['address'],
				'state_name'		=>	$opp_res['state_name'],
				'city_name'			=>	$opp_res['city_name'],	
				'status_flag'		=>	$opp_res['status_flag']	
				
			];
			
			array_push($array,$out);
			$i++;
		}
		
		return $result = ['content'=>$array,'status'=>'success','message'=>'list of requested opportunity'];
	}
		public function actionEdit_volunteer_profile()
	{
		$post = file_get_contents("php://input");
	    $request 		= Json::decode($post);
		$first_name 	= 	$_POST['first_name'];
		$last_name 		= 	$_POST['last_name'];
		$phone 			= 	$_POST['phone'];
		$dob 			= 	$_POST['dob'];
		$designation 	= 	$_POST['designation'];
		$organization 	= 	$_POST['organization']; 
		$user_id 		= 	$_POST['user_id'];
		$skills 		= 	$_POST['skills'];
		$interests		=	$_POST['interests'];
	    $model = User::find()->where(['id'=>$user_id ])->one(); 
		if(empty($model))
		{
			return $result = ['status'=>'fail','message'=>'No User found'];
		}
		$images = UploadedFile::getInstancesByName("file");
        if(!empty($images))
        {
		    foreach ($images as $image) {

		        $filename = $image->name;
		        $path = "../../profile_images/" . $filename;
		        $image->saveAs($path);//saving image
		    }
        }
        else{
        		$filename = $model->image_path;
        }
		$image_path = "http://staging.api.letstagon.com/profile_images/";
		$user_name 	= $first_name .' '.$last_name;
		$model->first_name		=	$first_name;
		$model->last_name		=	$last_name;
		$model->phone			=	$phone;
		$model->dob			=	$dob;
		$model->designation	=	$designation;
		$model->organization	=	$organization;
		$model->image_path     =    $filename;
		$model->skills 			=	$skills;
		$model->interest 		=   $interests;
	    $model->save(false);
		$out=['userid'=>$user_id,'user_name'=>$user_name,'image_path'=>$image_path.$filename];
		return $result = ['data'=>$out,'status'=>'success','message'=>'User Profile Updated Successfully'];
	}
	
	public function actionReviewslistbyevent(){
		$post = file_get_contents("php://input");
	    $request 		= Json::decode($post);
		$exp_id = $request['exp_id'];
		$review_list = "SELECT r.subject,r.message,r.rating,r.created_at,u.first_name,u.last_name,u.image_path,e.date_scheduled 
					FROM reviews as r
                        JOIN user as u ON 
							r.user_id = u.id
                        JOIN events as e ON 
                            r.event_id = e.id
						where r.experience_id = '".$exp_id."'";
		$command = Yii::$app->db->createCommand($review_list);
		$review_list_result = $command->queryAll();
		if(empty($review_list_result)){
			return $result = ['status'=>'fail','message'=>'No reviews for this event'];
		}
		$array=[];
		$image_path = "http://staging.api.letstagon.com/profile_images/";
		foreach($review_list_result as $review){
			
			$out=[
					'username'=>$review['first_name'].' '.$review['last_name'],
					'image_path'=>$image_path.$review['image_path'],
					'subject'=>$review['subject'],
					'message'=>$review['message'],
					'rating'=>$review['rating'],
					'date'=>$review['date_scheduled'],
					'published_date'=>$review['created_at']
			];
			
			array_push($array,$out);
		}
		return $result = ['data'=>$array,'status'=>'success','message'=>'Reviews List'];
	}
	
public function actionRegister_test()
{
	 $post = file_get_contents("php://input");
     $request = Json::decode($post);
	 
	 $first_name 	= $request['first_name'];
	 $last_name 	= "";
	 $email 		= $request['email'];
	 $password 		= $request['password'];
	 $phone 		= $request['phone'];
	 $dob 			= "";
	 $designation 	= "";
	 $organization 	= ""; 
	 $role_type 	= '2';
	 $status 		= '0';
	 $user_name 	= $first_name .' '.$last_name;
	 
	 $identity =  User::find()->where("`email`" . " ='" . $email ."'")->one();
	 
	 if(empty($identity)){
		 
		 $user = new User();  // Storing new model record in a variable
		 $user->first_name		=	$first_name;
		 $user->last_name		=	$last_name;
		 $user->email			=	$email;
		 $user->phone			=	$phone;
		 $user->dob				=	$dob;
		 $user->designation		=	$designation;
		 $user->organization	=	$organization;
		 $user->role_type		=	$role_type;
		 $user->status			=	$status;
		 $user->image_path		=   "default_icon.jpg";
		
		 $user->password_hash = Yii::$app->getSecurity()->generatePasswordHash($password);
        
		  $user->generateAuthKey(); 
		
		 $user->save(false); // To exceute the query
		 
		 $user_id =  $user->auth_key;
		   
		 /* Sending mail confitrmation using user model*/
		 return $result = ['status' => "success", 'message' => 'Registration Succesfull'];
		 
	 }
	 else
	 {
		 return $result = ['status' => "fail", 'message' => 'Email Id already exists'];
		 
	 } 
	 
}
	public function actionInstitution_request()
 {

	 $post = file_get_contents("php://input");
     $request = Json::decode($post);
	 $name 	= $request['name'];
	 $type 	= $request['type'];
	 $contact_person = $request['contact_person'];
	 $contact_number = $request['contact_number'];
	 $email = $request['email'];
	 $description = $request['description'];
	 $message = "<table border='1' cellpadding='10' cellspacing='10' width='100%' height='100'>
				<tr>
				<td width='100%' bgcolor='#fff' height='30'><img class='logo_light' alt='Letstagon Logo' src='http://agathsya.com/letstagon/admin/Admin/assets/images/logo-text.png'></td>
				</tr>
				<tr>
				<td width='100%' height='60'>
				<p></p>
				<p></p>";
				$message.= "<b>".$name."</b> has requested letstagon for registration";
				$message .= '<li>Type : '.$type.'</li>';
				$message .= '<li>Contact person : '.$contact_person.'</li>';
				$message .= '<li>Contact Number : '.$contact_number.'</li>';
				$message .= '<li>Email : '.$email.'</li>';
				$message.= '<li>Description : '.$description.'</li>';
				$message .="
				</td>
				</tr>
				<tr>
				<td width='100%' cellpadding='1' cellspacing='1' bgcolor='#DFDFDF' height='1'>
				</td>
				</tr>
				</table>";
	
		
		 Yii::$app->mailer->compose()
		->setFrom(['support@letstagon.com'=>'LetsTagOn'])
		->setTo('info@letstagon.com')
		->setSubject('Enquiry Message')
		->setHtmlBody($message)
		->send(); 

		return $result = ['status'=>'success','message'=>'Thank you for your enquiry. We will get back to you soon.'];
	}
	 public function actionInsert_outside_oppourtunity()
    {
         $post = file_get_contents("php://input");
         $request = Json::decode($post);
         $name=$request['name'];
         $cause=$request['cause'];
         $description=$request['description'];
         $event_date=$request['event_date'];
         $address=$request['address'];
         $user_id=12;
	     $duration=$request['event_duration'];
        
         
         
        
         $outside_oppourtunity=new OutsideOppourtunity();
         $outside_oppourtunity->name=$name;
         $outside_oppourtunity->cause=$cause;
         $outside_oppourtunity->description=$description;
         $outside_oppourtunity->event_date=$event_date;
         $outside_oppourtunity->address=$address;
         $outside_oppourtunity->user_id=$user_id;
         $outside_oppourtunity->event_duration=$duration;
         $outside_oppourtunity->save(false);
		 
         return $result = ['status' => "success", 'message' => 'Outside opportunity Saved Succesfull'];
    }
	 public function actionView_outside_oppourtunity()
     {
        $outside_oppourtunity= OutsideOppourtunity::find()->orderBy('id DESC')->all();
       
 	   if(empty($outside_oppourtunity))
        {
            return $res=['status'=>'fail','message'=>'No outside oppourtunities found'];
        }
        return $out=['data'=> $outside_oppourtunity,'status'=>'success','message'=>'List of outside_oppourtunities'];
               
     }
	 
	public function actionView_individual_outside_oppourtunity()
    {
          $post = file_get_contents("php://input");
          $request = Json::decode($post);
          $id=$request['id'];
          
          $outside_oppourtunity= OutsideOppourtunity::findOne($id);
		  
          if(empty($outside_oppourtunity))
          {
            return $res=['status'=>'fail','message'=>'No outside oppourtunities found'];
          }
         return $out=['data'=> $outside_oppourtunity,'status'=>'success','message'=>'List of outside oppourtunities'];
                
    }
      public function actionEdit_outside_oppourtunity()
    {
         $post = file_get_contents("php://input");
         $request = Json::decode($post);
        
         $name=$request['name'];
         $event_date=$request['event_date'];
         $address=$request['address'];
         $description=$request['description'];
         $cause=$request['cause'];
         $event_duration=$request['event_duration'];
	
         $date_schedule = date('Y-m-d H:i', strtotime($event_date));
         $outside_oppourtunity= OutsideOppourtunity::findOne($id);
         
         if(empty($outside_oppourtunity))
        {
            return $res=['status'=>'fail','message'=>'No outside oppourtunities found'];
        }
        $outside_oppourtunity->name=$name;
        $outside_oppourtunity->description=$description;
        $outside_oppourtunity->cause=$cause;
        $outside_oppourtunity->event_date=$date_schedule;
        $outside_oppourtunity->event_duration=$event_duration;
        $outside_oppourtunity->save(false);
        
        return $out=['data'=> $outside_oppourtunity,'status'=>'success','message'=>'Edit outside oppourtunities successfull'];
    }
	 public function  actionDelete_outside_oppourtunity()
    {
         $post = file_get_contents("php://input");
         $request = Json::decode($post);
         $id=$request['id'];
         $outside_oppourtunities= OutsideOppourtunity::find()->where(['id'=>$id])->one();
         
         if(empty($outside_oppourtunities))
         {
             return $res=['status'=>'fail','message'=>'No outside oppourtunities found'];
         }
         
        $outside_oppourtunities->delete();
         
        return $res=['status'=>'success','message'=>'outside opportunity deleted'];
    }
	public function actionOppourtunity_details()
    {
         $post = file_get_contents("php://input");
         $request = Json::decode($post);
         $id=$request['id'];
         $opp_info=OutsideOppourtunity::find()->where(['id'=>$id])->one();
         if(empty($opp_info))
         {
             return $res=['status'=>'fail','message'=>'not found'];
         }
         $opp= OutsideOppourtunity::find()->select(['id','name','address','cause','description','event_date','event_duration','is_active','created_at','updated_at'])->where(['id'=>$id])->one();
        
            $out=
             [
            'id'=>$opp['id'],
            'name'=>$opp['name'],
            'address'=>$opp['address'],
            'cause'=>$opp['cause'],
            'description'=>$opp['description'],
             'event_date'=>$opp['event_date'],
              'event_duration'=>$opp['event_duration'],
                'is_active'=>$opp['is_active'],
                'updated_at'=>$opp['updated_at'],
                'created_at'=>$opp['created_at']
                ];
        
        return $res=['data'=>$out,'status'=>'success'];
    
    
}
	public function actionView_category()
	{
		$category_details=ExperienceCategory::find()->select(['id','category_name'])->all();
		if(empty($category_details))
		{
			return $res=['status'=>'fail','message'=>'No categories found'];
        }
		return $res=['status'=>$category_details,'message'=>'List of categories'];

    }
	
}
