<?php

namespace api\v1\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $phone
 * @property string $dob
 * @property string $password_hash
 * @property string $auth_key
 * @property string $designation
 * @property string $organization
 * @property int $role_type
 * @property string $image_path
 * @property string $skills
 * @property double $lat
 * @property double $lng
 * @property string $interest
 * @property int $status 0-Inactive,1-Active
 * @property int $social_media_flag
 * @property string $app_id
 * @property int $reset_flag
 * @property int $IsActive
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 *
 * @property Address[] $addresses
 * @property LtgRequestedOpportunities[] $ltgRequestedOpportunities
 * @property Reviews[] $reviews
 * @property Testimonials[] $testimonials
 * @property MasterRoleTypes $roleType
 * @property UserTaggedExperiences[] $userTaggedExperiences
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'email', 'phone', 'password_hash', 'auth_key', 'role_type', 'image_path', 'skills', 'lat', 'lng', 'interest', 'social_media_flag', 'app_id', 'reset_flag', 'IsActive'], 'required'],
            [['role_type', 'status', 'social_media_flag', 'reset_flag', 'IsActive', 'created_by', 'updated_by'], 'integer'],
            [['lat', 'lng'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['first_name', 'last_name', 'email', 'phone', 'password_hash', 'organization', 'image_path', 'skills', 'interest', 'app_id'], 'string', 'max' => 255],
            [['dob'], 'string', 'max' => 25],
            [['auth_key', 'designation'], 'string', 'max' => 100],
            [['role_type'], 'exist', 'skipOnError' => true, 'targetClass' => MasterRoleTypes::className(), 'targetAttribute' => ['role_type' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'dob' => 'Dob',
            'password_hash' => 'Password Hash',
            'auth_key' => 'Auth Key',
            'designation' => 'Designation',
            'organization' => 'Organization',
            'role_type' => 'Role Type',
            'image_path' => 'Image Path',
            'skills' => 'Skills',
            'lat' => 'Lat',
            'lng' => 'Lng',
            'interest' => 'Interest',
            'status' => 'Status',
            'social_media_flag' => 'Social Media Flag',
            'app_id' => 'App ID',
            'reset_flag' => 'Reset Flag',
            'IsActive' => 'Is Active',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(Address::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLtgRequestedOpportunities()
    {
        return $this->hasMany(LtgRequestedOpportunities::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(Reviews::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTestimonials()
    {
        return $this->hasMany(Testimonials::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoleType()
    {
        return $this->hasOne(MasterRoleTypes::className(), ['id' => 'role_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserTaggedExperiences()
    {
        return $this->hasMany(UserTaggedExperiences::className(), ['user_id' => 'id']);
    }
		public function getAuthKey() {
        return $this->auth_key;
    }
	
	
	/**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey() {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
	
	/**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password) {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }
	
	/**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
	 
    public function validatePassword($password)
	{
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }
	
	/* For Register Success Email Delivery */
	
}
