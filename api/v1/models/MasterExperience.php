<?php

namespace api\v1\models;

use Yii;

/**
 * This is the model class for table "master_experience".
 *
 * @property int $id
 * @property string $experience_name
 * @property string $cause
 * @property string $category
 * @property string $description
 * @property string $image_path
 * @property int $ngo_id
 * @property int $status 0-Inactive,1-Active
 * @property int $IsActive
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 *
 * @property Events[] $events
 * @property ExperiencesLocation[] $experiencesLocations
 * @property Keywords[] $keywords
 * @property MasterNgo $ngo
 * @property Reviews[] $reviews
 */
class MasterExperience extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_experience';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['experience_name', 'cause', 'category', 'description', 'image_path', 'ngo_id', 'status', 'IsActive'], 'required'],
            [['description'], 'string'],
            [['ngo_id', 'status', 'IsActive', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['experience_name', 'cause', 'category', 'image_path'], 'string', 'max' => 255],
            [['ngo_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterNgo::className(), 'targetAttribute' => ['ngo_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'experience_name' => 'Experience Name',
            'cause' => 'Cause',
            'category' => 'Category',
            'description' => 'Description',
            'image_path' => 'Image Path',
            'ngo_id' => 'Ngo ID',
            'status' => 'Status',
            'IsActive' => 'Is Active',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Events::className(), ['experience_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExperiencesLocations()
    {
        return $this->hasMany(ExperiencesLocation::className(), ['experience_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKeywords()
    {
        return $this->hasMany(Keywords::className(), ['experience_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNgo()
    {
        return $this->hasOne(MasterNgo::className(), ['id' => 'ngo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(Reviews::className(), ['experience_id' => 'id']);
    }
}
