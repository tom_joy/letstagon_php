<?php

namespace api\v1\models;

use Yii;

/**
 * This is the model class for table "master_ngo".
 *
 * @property int $id
 * @property string $ngo_name
 * @property string $ngo_type
 * @property string $description
 * @property string $contact_person
 * @property string $contact_number
 * @property string $email
 * @property string $address
 * @property int $country_id
 * @property int $state_id
 * @property int $city_id
 * @property int $isActive
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 *
 * @property Country $country
 * @property States $state
 * @property Cities $city
 */
class MasterNgo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_ngo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ngo_name', 'ngo_type', 'description', 'contact_person', 'contact_number', 'email', 'address', 'country_id', 'state_id', 'city_id', 'isActive'], 'required'],
            [['description', 'address'], 'string'],
            [['country_id', 'state_id', 'city_id', 'isActive', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['ngo_name', 'ngo_type', 'contact_person', 'email'], 'string', 'max' => 255],
            [['contact_number'], 'string', 'max' => 15],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['state_id'], 'exist', 'skipOnError' => true, 'targetClass' => States::className(), 'targetAttribute' => ['state_id' => 'id']],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ngo_name' => 'Ngo Name',
            'ngo_type' => 'Ngo Type',
            'description' => 'Description',
            'contact_person' => 'Contact Person',
            'contact_number' => 'Contact Number',
            'email' => 'Email',
            'address' => 'Address',
            'country_id' => 'Country ID',
            'state_id' => 'State ID',
            'city_id' => 'City ID',
            'isActive' => 'Is Active',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(States::className(), ['id' => 'state_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id']);
    }
}
