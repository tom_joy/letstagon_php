<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'api\v1\controllers',
    'modules' => [
        'datecontrol' => [
            'class' => 'kartik\datecontrol\Module',
        ],
    ],
    'components' => [
        'request' => [
             'enableCsrfValidation' => false,
        ],
		'assetManager' => [
			 'linkAssets' => false,
		   ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
			'autoRenewCookie' => true,
            'identityCookie' => ['name' => '_identity-api', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the tiyoapi
            'name' => 'advanced-api',
        ],
        'response' => [
            'format' => yii\web\Response::FORMAT_JSON,
            'charset' => 'UTF-8',
        ], 
        'request' => [
            'class' => '\yii\web\Request',
            'enableCookieValidation' => false,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    		'authClientCollection' => [
    				'class' => 'yii\authclient\Collection',
    				'clients' => [
    						'google' => [
    								'class' => 'yii\authclient\clients\Google',
    								'clientId' => '973765919053-3scn73bu3qr4t2t98t2dh37n52lasr67.apps.googleusercontent.com',
    								'clientSecret' => '_Sy8ItCBkbCv2DpRLwa31laH',
    								'scope' => 'https://www.googleapis.com/auth/plus.login',
    								'returnUrl' => 'https://localhost/tiyoDevops/frontend/web/',
    						],
    						'facebook' => [
    								'class' => 'yii\authclient\clients\Facebook',
    								'authUrl' => 'https://www.facebook.com/dialog/oauth?display=popup',
    								'clientId' => '1626576554317329',
    								'clientSecret' => '02ac4693e374d9ca47a410d7c134dbd7',
    						],
    				],
    		],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'showScriptName' => false,
        	'enableStrictParsing' => true,
            'enablePrettyUrl' => true,
            'rules' => array(
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ],
    ],
    'params' => $params,
	'timeZone' => 'Asia/Kolkata',
];
