<?php

namespace api\v1\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\db\Query;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\helpers\Json;
use yii\db\Command;
use yii\web\UploadedFile;
use api\v1\models\About;
use api\v1\models\Contact;


class CmsController extends ActiveController {

    public $modelClass = 'api\v1\models\Contact';  // Define a model class name for the controller

public function behaviors()
{
    return [
        'corsFilter' => [
            'class' => \yii\filters\Cors::className(),
        ],
    ];
}
  /****** Contact Us Message API's Starts Here *******/

	public function actionInsert_contact_message()
	{
		 $post = file_get_contents("php://input");
		 $request = Json::decode($post);
		 $name 			= $request['name'];
		 $email 		= $request['email'];
		 $subject 		= "Contact query";
		 $message_text 	= $request['message'];
		 $phone 		= $request['phone'];
		 $contact 			= 	new Contact();  // Storing new model record in a variable
			 $contact->name		=	$name;
			 $contact->email	=	$email;
			 $contact->subject	=	$subject;
			 $contact->message	=	$message_text;
			 $contact->phone    =   $phone;
			 
			 $contact->save(false); // To exceute the query
			 
			 
			 
			 $message_admin = "<table border='1' cellpadding='10' cellspacing='10' width='100%' height='100'>
				<tr>
				<td><img class='logo_light' alt='Letstagon Logo' src='http://agathsya.com/letstagon/admin/Admin/assets/images/logo-icon.png'><img class='logo_light' alt='Letstagon Logo' src='http://agathsya.com/letstagon/admin/Admin/assets/images/logo-text.png'></td>
				</tr>
				<tr>
				<td width='100%' height='60'>
				<p></p>
				<p></p>";
				  
				$message_admin .= 'Dear Admin,<br><br>';
				$message_admin .= '<p style="font-size:16px;line-height:20px;padding:0;margin:0;margin-bottom:16px">A User has chosen to Contact Us over a query.</p>';
				$message_admin .= '<p><p style="font-size:16px;line-height:20px;padding:0;margin:0;margin-bottom:16px">
							Please find the details below:
						</p> 
						<p>
						<table style="font-weight:bold;">
							<tr>	   
								<td>Email Id </td>
								<td>'.$email.' </td>
							</tr>
							<tr>
								<td>User Name </td>
								<td>'.$name.' </td>
							</tr>
							<tr>
								<td>Contact Number </td>
								<td>'.$phone.' </td>
							</tr>
							<tr>
								<td>Contact Query </td>
								<td>'.$message_text.' </td>
							</tr>
						</table></p>';
						 
				$message_admin .= '<p style="font-size:16px;line-height:20px;padding:0;margin:0;margin-bottom:16px"><b>Happy volunteering,</b></p>';
				$message_admin .= '<p style="font-size:16px;line-height:20px;padding:0;margin:0;margin-bottom:16px"><b>LetsTagOn team</b></p>';
				$message_admin .="
				</td>
				</tr>
				<tr>
				<td width='100%' cellpadding='1' cellspacing='1' bgcolor='#DFDFDF' height='1'>
				</td>
				</tr>
				</table>";
		 
		  
		 
		 Yii::$app->mailer->compose()
		->setFrom(['support@letstagon.com'=>'LetsTagOn'])
		->setTo(['support@letstagon.com'=>'LetsTagOn'])
		->setSubject('Get In Touch - LetsTagOn')
		->setHtmlBody($message_admin)
		->send(); 
		
			 
		/************  User email below ****************/	 
			 
			 
			 $message = "<table border='1' cellpadding='10' cellspacing='10' width='100%' height='100'>
				<tr>
				<td><img class='logo_light' alt='Letstagon Logo' src='http://agathsya.com/letstagon/admin/Admin/assets/images/logo-icon.png'><img class='logo_light' alt='Letstagon Logo' src='http://agathsya.com/letstagon/admin/Admin/assets/images/logo-text.png'></td>
				</tr>
				<tr>
				<td width='100%' height='60'>
				<p></p>
				<p></p>";
				$message .= 'Greetings '.$name.',<br><br>';
				$message .= '<p>Thank you for contacting us. We appreciate your efforts in taking time to contact us.</p>';
				$message .= "<p>We shall revert over your query as soon as possible.</p>";
				$message .= '<p><b>Happy volunteering,</b></p>';
				$message .= '<p><b>LetsTagOn team</b></p>';
				$message .="
				</td>
				</tr>
				<tr>
				<td width='100%' cellpadding='1' cellspacing='1' bgcolor='#DFDFDF' height='1'>
				</td>
				</tr>
				</table>";
		 
		  
		 
		 Yii::$app->mailer->compose()
		->setFrom(['support@letstagon.com'=>'LetsTagOn'])
		->setTo($email)
		->setSubject('Get In Touch - LetsTagOn')
		->setHtmlBody($message)
		->send(); 
		
			
		
			 return $result = ['status' => "success", 'message' => 'Contact Message Saved Succesfull'];
		  
	}

	/*Api to View data */
	public function actionContact_message_list()
	{
		$contact = Contact::find()->select(['id','email','name','subject','message'])->all();
		if(empty($contact)){
			return $result = ['status'=>'fail','message'=>'No data'];
		}
		return $result = ['content'=>$contact,'status'=>'success','message'=>'list of contact messages'];
	}
 
 
	/*Api to Delete contact messages */
	public function actionDelete_contact_message()
	{
		 $post = file_get_contents("php://input");
		 $request = Json::decode($post);
	 
		$contact_id 	= $request['contact_id'];
		
		$contact = Contact::find()->where(['id'=>$contact_id])->one();
		if(empty($contact)){
			return $result = ['status'=>'fail','message'=>'No data'];
		}
		$contact->delete();
		return $result = ['status'=>'success','message'=>'Deleted Successfully'];
	}


 
  /****** Contact Us Message API's Ends Here *******/
 
 
 
 /****** ABOUT US API's Starts Here *******/
 
 
	/*Api to create about data */
	public function actionAbout()
	{
		 $post = file_get_contents("php://input");
	     $request = Json::decode($post);
		 
	     $title 		= $request['title'];
	     $description 	= $request['description'];
		 
	     $model = new About();
	     $model->title 		 = $title;
	     $model->description = $description;
	     $model->save(false);//To execute query
	     return $result = ['status'=>'success','message'=>'About content'];
	}
	
	/*Api to View data */
	public function actionViewabout()
	{
		$about = About::find()->select(['title','description'])->one();
		if(empty($about)){
			return $result = ['status'=>'fail','message'=>'No data'];
		}
		return $result = ['content'=>$about,'status'=>'success','message'=>'About data'];
	}
	
	/*Api to Edit About data */
	public function actionEditabout()
	{
		$post = file_get_contents("php://input");
	    $request 		= Json::decode($post);
	    $title 			= $request['title'];
	    $description 	= $request['description'];
	    $model = About::find()->where(['id'=>1])->one();
	    $model->title = $title;
	    $model->description = $description;
	    $model->save(false);
		return $result = ['status'=>'success','message'=>'Updated Successfully'];
	}

	/* Api to Delete About data */
	public function actionDeleteabout()
	{
		$about = About::find()->where(['id'=>1])->one();
		if(empty($about)){
			return $result = ['status'=>'fail','message'=>'No data'];
		}
		$about->delete();
		return $result = ['status'=>'success','message'=>'Deleted Successfully'];
	}

 /****** ABOUT US API's Ends Here *******/

}