import { Component, OnInit } from '@angular/core';
declare var $: any;
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AdminService } from "../service/admin.service";
import { empty } from '../../../node_modules/rxjs';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css'],
  providers: [AdminService]

})
export class EditProfileComponent implements OnInit {
  
  userForm: FormGroup;
  userid;
  submitted = false;
  constructor(private formBuilder: FormBuilder, private router: Router, private adminService: AdminService) { }

  ngOnInit() {
    $('.loading-spinner').show();
		this.userid = localStorage.getItem("userid");
		if (!this.userid) {
      $('.loading-spinner').hide();
      alert("Invalid action.");
      this.router.navigate(['view-profile']);
			return;
    }

    this.userForm = this.formBuilder.group({
			first_name: ['',],
			last_name: ['',],
      phone: [''],
			email: ['', [Validators.required, Validators.email]],
      dob: [''],
      user_id:[''],
			designation: [''],
      organization: [''],
      file:['']
    });
    this.adminService.getprofile(this.userid)
    .subscribe((res: any) => {
      if (res.status == "success") {
        $('.loading-spinner').hide();
        this.userForm.setValue(res.data);
      }
      else {
        $('.loading-spinner').hide();
        alert(res.messsage);
      }
    });
  }
// convenience getter for easy access to form fields
get f() { return this.userForm.controls; }
  onSubmit() {
    
    $('.loading-spinner').show();
    // stop here if form is invalid
    console.log(this.userForm.value);
    if (this.userForm.invalid) {
      alert("ok");
      $('.loading-spinner').hide();
      return;
      
    }
    alert("ok");
    console.log(this.userForm.value);
    this.adminService.geteditprofile(this.userForm.value).subscribe((res: any) => {
      if (res.status == "success") {
        $('.loading-spinner').hide();
        this.router.navigate(['/', 'view-profile']);
      }
      else {
        $('.loading-spinner').hide();
        alert(res.message);
      }
    });
  }
}
