<?php

namespace api\v1\models;

use Yii;

/**
 * This is the model class for table "outside_oppourtunity".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $address
 * @property string $event_date
 * @property string $cause
 * @property string $description
 * @property int $event_duration
 * @property int $is_active
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $user
 */
class OutsideOppourtunity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'outside_oppourtunity';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'name', 'address', 'event_date', 'cause', 'description', 'event_duration'], 'required'],
            [['user_id', 'event_duration', 'is_active'], 'integer'],
            [['address', 'description'], 'string'],
            [['event_date', 'created_at', 'updated_at'], 'safe'],
            [['name', 'cause'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'address' => 'Address',
            'event_date' => 'Event Date',
            'cause' => 'Cause',
            'description' => 'Description',
            'event_duration' => 'Event Duration',
            'is_active' => 'Is Active',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
