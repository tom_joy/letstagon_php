<?php

namespace api\v1\models;

use Yii;

/**
 * This is the model class for table "reviews".
 *
 * @property int $id
 * @property int $experience_id
 * @property int $event_id
 * @property int $user_id
 * @property string $subject
 * @property string $message
 * @property int $rating
 * @property int $status 0-Inactive,1-Active
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 *
 * @property MasterExperience $experience
 * @property User $user
 * @property Events $event
 */
class Reviews extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reviews';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['experience_id', 'event_id', 'user_id', 'subject', 'message', 'rating', 'status', 'created_by', 'updated_by'], 'required'],
            [['experience_id', 'event_id', 'user_id', 'rating', 'status', 'created_by', 'updated_by'], 'integer'],
            [['message'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['subject'], 'string', 'max' => 255],
            [['experience_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterExperience::className(), 'targetAttribute' => ['experience_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['event_id'], 'exist', 'skipOnError' => true, 'targetClass' => Events::className(), 'targetAttribute' => ['event_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'experience_id' => 'Experience ID',
            'event_id' => 'Event ID',
            'user_id' => 'User ID',
            'subject' => 'Subject',
            'message' => 'Message',
            'rating' => 'Rating',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExperience()
    {
        return $this->hasOne(MasterExperience::className(), ['id' => 'experience_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Events::className(), ['id' => 'event_id']);
    }
}
