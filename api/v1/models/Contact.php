<?php

namespace api\v1\models;

use Yii;

/**
 * This is the model class for table "contact".
 *
 * @property int $id
 * @property string $email
 * @property string $name
 * @property string $subject
 * @property string $message
 * @property string $date_of_contact
 */
class Contact extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'name', 'subject', 'message', 'date_of_contact'], 'required'],
            [['message'], 'string'],
            [['date_of_contact'], 'safe'],
            [['email', 'name', 'subject','phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'name' => 'Name',
            'subject' => 'Subject',
            'message' => 'Message',
			'phone' =>'Phone',
            'date_of_contact' => 'Date Of Contact',
        ];
    }
}
