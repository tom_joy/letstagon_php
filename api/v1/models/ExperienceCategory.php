<?php

namespace api\v1\models;

use Yii;

/**
 * This is the model class for table "experience_category".
 *
 * @property int $id
 * @property string $category_name
 * @property string $category_image
 * @property int $status
 * @property int $IsActive
 */
class ExperienceCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'experience_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_name', 'category_image'], 'required'],
            [['status', 'IsActive'], 'integer'],
            [['category_name', 'category_image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_name' => 'Category Name',
            'category_image' => 'Category Image',
            'status' => 'Status',
            'IsActive' => 'Is Active',
        ];
    }
}
